<?
namespace Mlife\Portal\Chatbot\Contecst;

use Mlife\Portal\Chatbot\Convert as Convert;
use Mlife\Portal\Chatbot\Log as Log;

class Operator implements \Mlife\Portal\Chatbot\Interfaces\Contecst{
	
	public static function send($text=''){
		
		
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		$command = self::getCommandData($text);
		
		$mess = array(
			
		);
		
		if($command == 'ping'){
			return false;
		}elseif($command == '_exit'){
			$session->setContecst('Start', true);
			$session->setParam('LAST_COMMAND','', true);
			$contecstClass = $session->getContecst();
			return $contecstClass::send($text);
		}
		
		if($command){
			
		}elseif($session->getParam('LAST_COMMAND') == '_operator'){
			$session->setParam('LAST_COMMAND','', true);
			$session->setParam('SESSION_EXPIRED',2*24*60*60, true);
			
			$mess = array(
				'text' => 'Теперь вы можете написать запрос в службу поддержки. Что желаете спросить?'."\n\n".'Оператор ответит вам в ближайшее время.'."\n".
				'Время работы службы поддержки: пн-пт с 9-00 до 16-00.'."\n".
				'Для выхода в главное меню, отправьте команду: выход'
				,
				'tts' => 'Спасибо за ваше обращение.',
				'buttons' => array()
			);
			
		}
		
		Log::add($mess, 'Сформированный ответ на команду '.$command);
		
		return $mess;
		
	}
	
	public static function getCommandData($text=''){
		
		Log::add($text, 'Контекст '.self::class);
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		$text = Convert::toLowerText($text);
		
		$command = '';
		
		if($text == 'ping') {
			$command = 'ping';
			return $command;
		}elseif($text=='выход') {
			$command = '_exit';
		}elseif($text=='выход в меню') {
			$command = '_exit';
		}elseif($text == '-'){
			$command = '_exit';
		}
		
		Log::add($text, 'Определение команды '.$command);
		
		return $command;
	}
	
}