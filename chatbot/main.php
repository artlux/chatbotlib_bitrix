<?
namespace Mlife\Portal\Chatbot;

use Mlife\Portal\Chatbot\Log as Log;

class Main{
	
	public static $data = array();
	public static $user = null;
	public static $transport = null;
	
	function __construct($transport, $data = array(), $defaultParams = array()) {
		
		if(!$transport['CONFIG']) $transport['CONFIG'] = array();
		$className = '\\Mlife\\Portal\\Chatbot\\Transport\\'.$transport['CLASS'];
		
		self::$transport = new $className($transport['CONFIG']);
		
		$data = array_merge($data,self::$transport->getConfigSession());
		self::$data = $data;
		
		$defaultParams = array_merge($defaultParams, self::$transport->getConfigUser());
		
		if($data['SESSION']['ID']){
			//if($this->getTransportName() == 'Bitrix24') $data['SESSION']['ID'] = md5($data['SESSION']['ID']);
			self::$user = new \Mlife\Portal\Chatbot\Usersession($data['SESSION']['ID'], $defaultParams, $data['SESSION']['EXPIRED']);
		}
		
	}
	
	public function send($data=array()){
		return self::$transport->request($data);
	}
	
	public function getTransportName(){
		return self::$transport->getName();
	}
	
	public function log($data, $title = ''){
		return Log::add($data, $title);
	}
	
}