<?
namespace Mlife\Portal\Chatbot;

class Convert {
	
	public static function getCp1251($text){
		return $GLOBALS["APPLICATION"]->ConvertCharset($text,  "utf-8", SITE_CHARSET);
	}
	public static function getUtf8Ar($data){
		return $GLOBALS["APPLICATION"]->ConvertCharsetArray($data,   SITE_CHARSET, "utf-8");
	}
	public static function getUtf8($data){
		return $GLOBALS["APPLICATION"]->ConvertCharset($data,   SITE_CHARSET, "utf-8");
	}
	
	public static function toLowerText($text){
		return self::getUtf8(toLower(self::getCp1251($text)));
	}
	
	public static function getGlobalCommand($text){
		
		if(in_array(str_replace('/','',$text),array('start','help','settings'))) {
			return array($text,'Start');
		}
		
		return false;
	}
	
	public static function minText($text){
		if(strlen($text)>1024){
			$text = str_replace(
				array('(прибытие)', '(отправление)', 'школа', 'магазин', 'поворот', 'перекресток', 'Примостовая', 'переулок', 'Магазин', 'микрорайон', '.', ' - ', '-й','Рынок','рынок','Маршрут:','Припятский'),
				array('прб', 'отп', 'шк', 'м', 'п', 'п-ок', 'Прим.', 'пер', 'м', 'м-н', '', '- ', '', '', '','м-т','Прип.'),
				$text
			);
		}
		return $text;
	}
	
	public static function resendFromVk($json=false,$url=false){
		
		if(!$json){
			$json = file_get_contents('php://input');
		}
		
		if(!$url) {
			if (!WEB_HOOK_URL) return false;
			$url = WEB_HOOK_URL;
		}
		
		Log::add($json, 'prepare json bitrix24');
		
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$httpClient->setHeader('Content-Type', 'application/json', true);
		$res = $httpClient->post($url,$json);
		
		Log::add($res, 'result bitrix24');
		
		return $res;
		
	}
	
	public static function resendFromTg($json=false,$url=false){
		
		if(!$json){
			$json = file_get_contents('php://input');
		}
		
		if(!$url) {
			if (!WEB_HOOK_URL) return false;
			$url = WEB_HOOK_URL;
		}
		
		Log::add($json, 'prepare json bitrix24');
		
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$httpClient->setHeader('Content-Type', 'application/json', true);
		$res = $httpClient->post($url,$json);
		
		Log::add($res, 'result bitrix24');
		
		return $res;
		
	}
	
	public static function resendFromViber($json=false,$url=false){
		
		if(!$json){
			$json = file_get_contents('php://input');
		}
		
		if(!$url) {
			if (!WEB_HOOK_URL) return false;
			$url = WEB_HOOK_URL;
		}
		
		Log::add($json, 'prepare json bitrix24');
		
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$httpClient->setHeader('Content-Type', 'application/json', true);
		$res = $httpClient->post($url,$json);
		
		Log::add($res, 'result bitrix24');
		
		return $res;
		
	}
	
}