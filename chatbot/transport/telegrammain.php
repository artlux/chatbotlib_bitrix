<?
namespace Mlife\Portal\Chatbot\Transport;

use Mlife\Portal\Chatbot\Log as Log;

class Telegrammain{
	
	public $config = array();
	public $appsConfig = array();
	public $cacheData = array();
	
	function __construct($config = array()) {
		
		$this->config = $config;
		
		try{
			$json = file_get_contents('php://input');
			$jsonData = json_decode($json);
			$this->cacheData = $jsonData;
			
			if($this->cacheData->callback_query){
				
				$this->cacheData->message = $this->cacheData->callback_query->message;
				$this->cacheData->message->text = $this->cacheData->callback_query->data;
				$this->cacheData->message->from = $this->cacheData->callback_query->from;
				
			}
			
			/*
			if($this->cacheData->message->text && strpos($this->cacheData->message->text, ']')!==false){
				$t = explode(']',$this->cacheData->message->text);
				$this->cacheData->message->text = $t[0];
			}
			*/
		
			$appsConfig = Array();
			if (file_exists(DEFAULT_DIR.'/config.php'))
				include(DEFAULT_DIR.'/config.php');
			
			$this->appsConfig = $appsConfig;
			
			if(empty($this->appsConfig)) {
				throw new \Bitrix\Main\ArgumentNullException("appsConfig");
			}
		
		}catch(\Exception $ex){
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			echo $ex->getMessage();
		}
		
	}
	
	public static function getName(){
		return static::class;
	}
	
	public function getConfigSession(){
		
		if(!isset($this->cacheData->message->from->id)) return array();
		
		return array(
			'SESSION' => array(
				'ID'=>md5($this->cacheData->message->from->id.'_'.$this->appsConfig['botid']).'_'.$this->appsConfig['botid'],
			)
		);
	}
	
	public function getConfigUser(){
		
		if(!isset($this->cacheData->message->from->id)) return array();
		
		return array(
			'USER_KEY'=>'telegramm_'.$this->cacheData->message->from->id,
			'CHAT_ID'=>$this->cacheData->message->chat->id
		);
	}
	
	public function request($data){
		
		if(empty($data)) $data = $this->cacheData;
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session === null) return $this->sendMessage(false);
		
		if($data->message->text && !$data->message->from->is_bot){
			
			//подставляем payload для значения текста в кнопке
			$lastMess = $session->getParam('LAST_MESS_DATA');
			$buttons_viber = array();
			$buttons = array();
			if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
			if($lastMess['buttons']) $buttons = $lastMess['buttons'];
			
			$buttons_viber[] = array(
				"title"=> 'маршруты',
				"payload"=>'покажи расписание'
			);
			$buttons_viber[] = array(
				"title"=> 'выход в меню',
				"payload"=>'выход'
			);
			
			Log::add($lastMess, 'ImBot lastMess');
			
			foreach($buttons_viber as $b){
				if(trim($b['title']) == trim($data->message->text)) {
					if($b['payload']) {
						$data->message->text = $b['payload'];
						break;
					}
				}
			}
			
			foreach($buttons as $b){
				if(trim($b['title_button']) == trim($data->message->text)) {
					if($b['payload']) {
						$data->message->text = $b['payload'];
						break;
					}
				}
			}
			
			$this->cacheData->message->text = $data->message->text;
			
			$mess = trim($data->message->text);
			
			$contecstClass = $session->getContecst();
			
			if(!$session->getParam('CONTECST')) {
				$session->setContecst('Start', true);
				$contecstClass = $session->getContecst();
			}
			
			if($arMess = \Mlife\Portal\Chatbot\Convert::getGlobalCommand($mess)) {
				$mess = $arMess[0];
				$session->setContecst($arMess[1], true);
				$contecstClass = $session->getContecst();
			}
			
			Log::add($data, 'ImBot Event Query');
			$mess = $contecstClass::send($mess);
			
			if(empty($mess) && $session->getParam('CONTECST') == 'Operator'){
				echo \Mlife\Portal\Chatbot\Convert::resendFromTg();
				return;
			}elseif($session->getParam('CONTECST') == 'Operator'){
				\Mlife\Portal\Chatbot\Convert::resendFromTg();
			}
			
			$result = $this->sendMessage($mess);
		
		}
		
		return $this->sendMessage(false);
		
	}
	
	public function sendMessage($mess){
		
		if($mess === false) {
			echo 'ok';
			return false;
		}
		
		if(!$mess['text']) {
			Log::add($mess, 'message is empty');
			return false;
		}
		
		$mess = $this->prepareData($mess);
		
		$url = 'https://api.telegram.org/bot'.$this->appsConfig['token'].'/sendMessage';
		$zapros = array(
			'chat_id'=>$this->cacheData->message->chat->id,
			'text'=>$mess['text']
		);
		
		if($mess['keyboard']){
			$mess['keyboard_fin'] = array(
				"one_time_keyboard" => true,
				"resize_keyboard" => true,
				'keyboard' => array(),
			);
			
			$prepare = array();
			$prepare_count = 0;
			foreach($mess['keyboard']['keyboard'] as $k=>$v){
				if(!$v['column']) $v['column'] = 6;
				if($v['column'] == 4) $v['column'] = 6;
				if($v['column'] == 5) $v['column'] = 6;
				if(strlen($v['text'])<6) $v['column'] = 2;
				if($v['text']=='ПН-ПТ') $v['column'] = 2;
				
				if(($prepare_count + $v['column'])>6 && !empty($prepare)){
					$mess['keyboard_fin']['keyboard'][] = $prepare;
					$prepare = array();
					$prepare_count = 0;
					$prepare[] = $v['text'];
					$prepare_count = $prepare_count + $v['column'];
				}else{
					$prepare[] = $v['text'];
					$prepare_count = $prepare_count + $v['column'];
				}
			}
			if(!empty($prepare)) $mess['keyboard_fin']['keyboard'][] = $prepare;
			
			$zapros['reply_markup'] = json_encode($mess['keyboard_fin']);
			
			Log::add($mess['keyboard_fin'],'format_key');
			Log::add($mess['keyboard']['keyboard'],'format_key_old');
			
		}else{
			$mess['keyboard_fin'] = array(
				"one_time_keyboard" => true,
				"resize_keyboard" => true,
				"keyboard"=>array(
					array('выход в меню')
				)
			);
			$zapros['reply_markup'] = json_encode($mess['keyboard_fin']);
		}
		
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$result = $httpClient->post($url, $zapros);
		
		Log::add($result, 'result rest telegramm sendMessage');
		
		if($this->cacheData->callback_query->id) echo json_encode(array('callback_query_id'=>$this->cacheData->callback_query->id));
		
		return $result;
		
	}
	
	private function prepareData($data){
		
		$mess = $data['text'];
		if(!$mess) return $data;
		
		/*if(!empty($data['buttons'])){
			$cm = array();
			foreach($data['buttons'] as $v){
				$cm[] = $v['title_comand'];
			}
			$mess .= "\n\nДоступны команды: ".implode('; ',$cm).".";
		}*/
		
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session->getParam('CONTECST') == 'Raspis'){
		
			$data['keyboard'] = array(
				"one_time_keyboard" => true,
				//"remove_keyboard" => true,
				//"selective"=> true,
				"resize_keyboard" => true,
				"keyboard"=>array(
					array(
						"text"=> 'маршруты',
						"callback_data"=>'покажи расписание',
						"column"=>3,
					),
					array(
						"text"=> 'выход',
						"callback_data"=>'выход',
						"column"=>3,
					)
				)
			);
			
			if(!empty($data['buttons_marsh'])){
				
				foreach($data['buttons_marsh'] as $b){
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload']
					);
				}
			}
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['keyboard'])>40) break;
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload'],
						"column"=>$b['columns'],
					);
				}
			}
			
			if(!empty($data['buttons'])){
				$col = 6;
				if(strlen($data['buttons'][0]['title_button'])<5) $col = 2;
				foreach($data['buttons'] as $v){
					$data['keyboard']['keyboard'][] = array(
						"text"=> $v['title_button'],
						"callback_data"=>$v['payload'],
						"column"=>$col,
					);
				}
				/*$cm = array();
				foreach($data['buttons'] as $v){
					$cm[] = $v['title_comand'];
				}
				$mess .= "\n\nДоступны команды: ".implode('; ',$cm).".";
				*/
			}
			
		}elseif($session->getParam('CONTECST') == 'Reklama'){
				
			$data['keyboard'] = array(
				"one_time_keyboard" => true,
				//"remove_keyboard" => true,
				//"selective"=> true,
				"resize_keyboard" => true,
			);
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['keyboard'])>40) break;
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload'],
						"column"=>$b['columns'],
					);
				}
			}
			
		}elseif($session->getParam('CONTECST') == 'Start'){
				
			$data['keyboard'] = array(
				"one_time_keyboard" => true,
				//"remove_keyboard" => true,
				//"selective"=> true,
				"resize_keyboard" => true,
			);
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['keyboard'])>40) break;
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload'],
						"column"=>$b['columns'],
					);
				}
			}
			
		}
		
		if(isset($data['keyboard']) && empty($data['keyboard']['keyboard'])) unset($data['keyboard']);
		
		if($session->getParam('CONTECST') == 'Raspis'){
			$mess .= "\n\nДля завершения диалога отправьте команду: выход.";
		}
		
		$data['text'] = $mess;
		
		return $data;
		
	}
	
}