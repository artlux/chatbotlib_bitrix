<?
namespace Mlife\Portal\Chatbot;

class Raspis {
	
	public static $lastJson = null;
	public static $api_type = null;
	
	public static function minText($text){
		if(strlen($text)>1024){
			$text = str_replace(
				array('(прибытие)', '(отправление)', 'школа', 'магазин', 'поворот', 'перекресток', 'Примостовая', 'переулок', 'Магазин', 'микрорайон', '.', ' - ', '-й','Рынок','рынок','Маршрут:','Припятский'),
				array('прб', 'отп', 'шк', 'м', 'п', 'п-ок', 'Прим.', 'пер', 'м', 'м-н', '', '- ', '', '', '','м-т','Прип.'),
				$text
			);
		}
		return $text;
	}
	
	public static function getCommandData($text='', $jsonData=false){
		
		if(!$text && $jsonData) {
			if($jsonData->request->payload) $text = $jsonData->request->payload;
		}
		
		$text = self::toLowerText($text);
		
		$sessionId = $jsonData->session->session_id;
		$userParams = self::currentUserData($sessionId);
		
		$command = '';
		
		if($text == 'ping') {
			$command = 'ping';
			return $command;
		}elseif(strpos($text,'завершить')!==false && self::$api_type=='vk') {
			$command = '_exit';
		}elseif(strpos($text,'отправить ошибку')!==false) {
			$command = '_error_send';
		}elseif(strpos($text,'ошибк')!==false) {
			$command = '_error';
			if($userParams->LAST_COMMAND == '_error') $command = '';
		}elseif(strpos($text,'тупица')!==false || strpos($text,'тупая')!==false) {
			$command = '_kompl';
		}elseif(strpos($text,'спасиб')!==false) {
			$command = '_thanks';
		}elseif(strpos($text,'расписание')!==false) {
			$command .= '_rasp';
			
			if(strpos($text,'маршрут')!==false || strpos($text,'автобус')!==false) $command .= '_marsh';
			if(strpos($text,'остановк')!==false) $command .= '_station';
			
		}
		
		self::writeToLog($text, 'Определение команды '.$command);
		
		if($jsonData && $command){
			
			//TRANS
			self::currentUserData($sessionId,array('SESSION'=>$sessionId,'LAST_COMMAND'=>$command, 'LAST_UID'=>$userParams->LAST_UID));
		}
		
		return $command;
	}
	
	
	public static function parseData($command, $text, $jsonData=false){
		
		if(!$text && $jsonData) {
			if($jsonData->request->payload) $text = $jsonData->request->payload;
		}
		
		$data = array();
		
		$text = self::toLowerText($text);
		
		if($command == '_rasp_marsh'){
			preg_match('/.*?(?:(?:автобус|маршрут).*?\s(.*?)|расписание\s([^\s]+)\s(?:автобус|маршрут).*?)$/i',$text,$data);
			if(count($data)==3){
				$data = array($data[0],($data[2] ? $data[2] : $data[1]));
			}
			//echo'<pre>';print_r($data);echo'</pre>';
			
		}
		if($command == '_rasp_station'){
			preg_match('/.*?остановк.*?\s(.*?)$/i',$text,$data);
		}
		if($command == '_rasp_marsh_station'){
			preg_match('/.*?(?:(?:автобус|маршрут).*?\s([^\s]+)|расписание\s([^\s]+)\s(?:автобус|маршрут).*?\s[на]{2}).*?\sостановк[^\s]+(.*?)$/i',$text,$data);
			if(count($data) == 4){
				$data = array($data[0],($data[1] ? $data[1] : $data[2]),$data[3]);
			}
			//preg_match('/.*?(?:расписание\s([^\s]+)\s(?:автобус|маршрут).*?\s[на]{2}).*?\sостановк[^\s]+(.*?)$/i',$text,$data);
			//([^\s]+)\s(?:автобус|маршрут).*?\s[на]{2}
		}
		
		if($jsonData && !$command){
			$userParams = self::currentUserData($jsonData->session->session_id);
			if($userParams->LAST_COMMAND){
				$data = array('LAST_COMMAND'=>$userParams->LAST_COMMAND,'TEXT'=>$text, 'LAST_UID'=>$userParams->LAST_UID);
			}
		}
		
		self::writeToLog($data, 'Разбор команды '.$command);
		return $data;
		
	}
	
	public static function getData($comand, $pAr, $jsonData){
			
			$data = array();
			
			if($comand == 'ping') {
				
				return array(
					'TEXT'=>"пинг запрос",
					'TTS'=>"пинг запрос"
				);
				
			}elseif($comand == '_exit') {
				
				return array(
					'TEXT'=>"Завершить диалог.",
					'TTS'=>"Завершить диалог."
				);
				
			}elseif($comand == '_kompl') {
				
				return array(
					'TEXT'=>"Спасибо за комплимент. А ведь я могу и подкрутить время в расписаниях.",
					'TTS'=>"Спасибо за комплимент. А ведь я могу и подкрутить время в расписаниях."
				);
				
			}elseif($comand == '_thanks') {
				
				return array(
					'TEXT'=>"Не за что. Обращайтесь, будем рады помочь.",
					'TTS'=>"Не за что. Обращайтесь, будем рады помочь."
				);
				
			}elseif($comand == '_error') {
				
				return array(
					'TEXT'=>"Напишите текст ошибки, для отправки данных в отдел разработки MLife.by",
					'TTS'=>"Напишите текст ошибки, для отправки данных в отдел разработки Эмлайф."
				);
				
			}elseif($comand == '_error_send'){
					
					return array(
						'TEXT'=>"Спасибо за обратную связь. Постараемся решить проблему в ближайшее время.",
						'TTS'=>"Спасибо за обратную связь. Постараемся решить проблему в ближайшее время.",
						'BUTTONS_DEFAULT'=>true
					);
					
			}elseif($comand == '_rasp') {
				
				return array(
					'TEXT'=>"Примеры запросов на получение расписаний: 
					- покажи расписание маршрута <14> 
					- покажи расписание на остановке <площадь ленина> 
					- покажи расписание маршрута <14> на остановке <площадь ленина> \n
					* в скобках можно менять данные на нужные Вам, названия остановок сообщайте в именительном падеже",
					'TTS'=>"Посмотрите примеры запросов на получение расписаний и повторите попытку."
				);
				
			}elseif($comand == '_rasp_station') {
				//print_r($pAr);
				if(count($pAr) == 2){
					$data = self::findStationId($pAr[1]);
					if(count($data)>1){
						$text = '';
						foreach($data as $cnt=>$station){
							$text .= "\n".$station['ID'].' - '.preg_replace('/\#.*\#/','',$station['NAME'])."\n".$station['DESC'];
						}
						return array(
							'TEXT'=>"Выберите номер остановки из списка:".$text,
							'TTS'=>"Выберите номер остановки из списка"
						);
					}elseif(count($data)==1){
						$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($data[0]['ID']);
						if($res){
							return self::getStationRasp($data[0]['ID'], $pAr['TEXT']);
						}else{
							return array(
								'TEXT'=>"Остановка с идентификатором ".$statId." не найдена",
								'TTS'=>"Остановка не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
							);
						}
					}else{
						return array(
							'TEXT'=>"Остановка \"".$pAr[1]."\" не найдена",
							'TTS'=>"Остановка не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
						);
					}
					//echo'<pre>';print_r($data);echo'</pre>';
				}
				
			}elseif($comand == '_rasp_marsh'){
				
				if(count($pAr) == 2){
					
					$data = self::findMarshId($pAr[1]);
					
					if(count($data)>0){
						$transports = array();
						$text = '';
						foreach($data as $marsh){
							foreach($marsh['TRANS_IDS'] as $trans){
								$transports[$trans] = $trans;
							}
							$text .= "\n\nМаршрут: ".$marsh['NAME'];
							foreach($marsh['STATIONS'] as $station){
								$text .= "\n".$station[0].' - '.preg_replace('/\#.*\#/','',$station[1]);
							}
						}
						
						$userParams = self::currentUserData($jsonData->session->session_id);
						self::currentUserData(
							$jsonData->session->session_id,
							array(
								'SESSION'=>$jsonData->session->session_id,
								'LAST_COMMAND'=>$userParams->LAST_COMMAND,
								'TRANS'=>implode(',',$transports),
								'LAST_UID'=>$userParams->LAST_UID
							)
						);
						
						return array(
							'TEXT'=>"Выберите номер остановки из списка:".$text,
							'TTS'=>"Выберите номер остановки из списка"
						);
					}else{
						return array(
							'TEXT'=>"Маршрут №:".$pAr[1]." не найден.",
							'TTS'=>"Маршрут №:".$pAr[1]." не найден.",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
						);
					}
					
				}
				
			}elseif($comand == '_rasp_marsh_station'){
				
				if(count($pAr) == 3){
					
					$data1 = self::findMarshId($pAr[1]);
					$stations = array();
					$transports = array();
					foreach($data1 as $marsh){
						foreach($marsh['TRANS_IDS'] as $trans){
							$transports[$trans] = $trans;
						}
						foreach($marsh['STATIONS'] as $station){
							$stations[] = $station[0];
						}
					}
					
					$userParams = self::currentUserData($jsonData->session->session_id);
					self::currentUserData(
						$jsonData->session->session_id,
						array(
							'SESSION'=>$jsonData->session->session_id,
							'LAST_COMMAND'=>$userParams->LAST_COMMAND,
							'TRANS'=>implode(',',$transports),
							'LAST_UID'=>$userParams->LAST_UID
						)
					);
					
					$data = self::findStationId($pAr[2], $stations);
					
					if(count($data)>1){
						$text = '';
						foreach($data as $cnt=>$station){
							$text .= "\n".$station['ID'].' - '.preg_replace('/\#.*\#/','',$station['NAME'])."\n".$station['DESC'];
						}
						return array(
							'TEXT'=>"Выберите номер остановки из списка:".$text,
							'TTS'=>"Выберите номер остановки из списка"
						);
					}elseif(count($data)==1){
						$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($data[0]['ID']);
						if($res){
							return self::getStationRasp($data[0]['ID'], $pAr['TEXT']);
						}else{
							return array(
								'TEXT'=>"Остановка с идентификатором ".$statId." не найдена",
								'TTS'=>"Остановка не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
							);
						}
					}else{
						return array(
							'TEXT'=>"Остановка \"".$pAr[2]."\" на маршруте \"".$pAr[1]."\" не найдена",
							'TTS'=>"Остановка \"".$pAr[2]."\" на маршруте \"".$pAr[1]."\" не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
						);
					}
					
				}
				
			}elseif(!$comand && isset($pAr['LAST_COMMAND'])){
				if($pAr['LAST_COMMAND'] == '_rasp_station'){
					$statId = preg_replace('/([^0-9])/is','',$pAr['TEXT']);
					$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($statId);
					if($res){
						$data = self::getStationRasp($statId, $pAr['TEXT']);
						return $data;
					}else{
						return array(
							'TEXT'=>"Остановка с идентификатором ".$statId." не найдена",
							'TTS'=>"Остановка не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
						);
					}
					
				}elseif($pAr['LAST_COMMAND'] == '_rasp_marsh_station'){
					$statId = preg_replace('/([^0-9])/is','',$pAr['TEXT']);
					$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($statId);
					if($res){
						$data = self::getStationRasp($statId, $pAr['TEXT']);
						return $data;
					}else{
						return array(
							'TEXT'=>"Остановка с идентификатором ".$statId." не найдена",
							'TTS'=>"Остановка не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
						);
					}
					
				}elseif($pAr['LAST_COMMAND'] == '_rasp_marsh'){
					$statId = preg_replace('/([^0-9])/is','',$pAr['TEXT']);
					$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($statId);
					if($res){
						$data = self::getStationRasp($statId, $pAr['TEXT']);
						return $data;
					}else{
						return array(
							'TEXT'=>"Остановка с идентификатором ".$statId." не найдена",
							'TTS'=>"Остановка не найдена",
								'BUTTONS'=>array(
									json_decode('{"payload":"сообщить об ошибке","title":"сообщить об ошибке","hide":"true"}'),
								)
						);
					}
					
				}elseif($pAr['LAST_COMMAND'] == '_error'){
					
					return array(
						'TEXT'=>"Дополните данные при необходимости и введите текст \"отправить ошибку\"",
						'TTS'=>"Дополните данные при необходимости и введите текст отправить ошибку",
						'BUTTONS'=>array(
							json_decode('{"payload":"отправить ошибку","title":"отправить ошибку","hide":"true"}'),
						)
					);
					
				}
			}
			
			return $data;
	}
	
	public static function getStationRasp($stationId, $text=''){
			
			$userParams = self::currentUserData(self::$lastJson->session->session_id);
			
			$allTransGroup = array();
			if(empty($allTransGroup)){
			$r2 = \Mlife\Portal\Raspisanie\TransgroupvaluesTable::getList(
				array(
					"select"=>array("TRANS","ID"),
				)
			);
			while($data = $r2->fetch()){
				$allTransGroup[$data["TRANS"]] = $data["ID"];
			}
			}
			
			//$stationId = str_replace("station_","",$page);
			
			
			
			$filter = array("TR.TIP"=>3, "STATION"=> $stationId);
			//if(!empty($userParams->TRANS)) $filter['TRANS'] = explode(',',$userParams->TRANS);
			
			//список маршрутов по станции
			$res = \Mlife\Portal\Raspisanie\TranstostatTable::getList(array(
				'select' => array("STATION","TRANS","NAME"=>"TR.NAME"),
				'runtime' => array(
					new \Bitrix\Main\Entity\ReferenceField('TR', '\Mlife\Portal\Raspisanie\TransportTable', 
						array('=this.TRANS' => 'ref.ID'), array("join_type"=>"left")
					),
				),
				'filter' => $filter
			));
			$arStationsMarsh = array();
			while($data = $res->fetch()){
				$num = preg_replace("/.*№([0-9aа]+).*?$/is","$1",$GLOBALS["APPLICATION"]->ConvertCharset($data["NAME"], SITE_CHARSET, "utf-8"));
				if($num) $arStationsMarsh[$num] = $num;
			}
			asort($arStationsMarsh,SORT_NUMERIC);
			
			//получение описания станций
			$res = \Mlife\Portal\Raspisanie\RaspStationTable::getById($stationId);
			$arResStation["CURRENT_STATION"] = $res->Fetch();
			
			$arResStation["ID"] = $arResStation["CURRENT_STATION"]["ID"];
			
			//получение маршрутов по данной станции
			$res = \Mlife\Portal\Raspisanie\TransportTable::getList(array(
				"select" => array("*"),
				"filter" => array("S_TO.STATION"=>$arResStation["CURRENT_STATION"]["ID"], "TIP"=>3),
				'runtime' => array(
					new \Bitrix\Main\Entity\ReferenceField('S_TO', '\Mlife\Portal\Raspisanie\TranstostatTable', 
						array('=this.ID' => 'ref.TRANS')
					),
				),
			));
			$arResStation["TRANS"] = array();
			while($arData = $res->fetch()){
				$arResStation["TRANS"][$arData["ID"]] = $arData;			
			}
			
			//получаем расписания
			$res = \Mlife\Portal\Raspisanie\RaspdatetimeTable::getList(array(
				"filter" => array(
					"STATION"=>$arResStation["CURRENT_STATION"]["ID"], 
					">S_TO.DATE_TO"=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time()),
					"S_TO.TRANS"=> array_keys($arResStation["TRANS"])
				),
				"runtime" => array(
					new \Bitrix\Main\Entity\ReferenceField('S_TO', '\Mlife\Portal\Raspisanie\RaspdateTable', 
						array('=this.RASP' => 'ref.ID')
					),
				),
				"select" => array("TO_"=>"S_TO.*","*"),
				"order" => array("TO_TIME_OTP"=>"ASC")
			));
			$arResStation["RASP"] = array();
			while($arData = $res->fetch()){
				$date = $arData["TO_TIME_OTP"]+($arData["TIME_PRIB"]*60);
				$arResStation["RASP"][$arData["TO_DAY"]][$arData["TO_TRANS"]][] = $date;
			}
			$arRes = array(
			'DAY_1'=>array('ITEMS'=>array()), 
			'DAY_4'=>array('ITEMS'=>array()), 
			'DAY_5'=>array('ITEMS'=>array()), 
			'DAY_2'=>array('ITEMS'=>array())
			);
			//echo '<pre>';print_r($arResStation["RASP"]);echo'</pre>';
			foreach($arResStation["RASP"] as $typeday=>$raspDay){
				if($typeday==1 || $typeday==2 || $typeday==4 || $typeday==5){
					//сортируем по количеству рейсов
					$newRasp = array();
					$raspDayTemp = array();
					foreach($raspDay as $transId=>$time){
						$newRasp[$transId] = count($time);
					}
					arsort($newRasp,SORT_NUMERIC);
					foreach($newRasp as $vv=>$tt){
						$raspDayTemp[$vv] = $raspDay[$vv];
					}
					$raspDay = $raspDayTemp;
					unset($raspDayTemp);
					unset($newRasp);
					
					$allArMarsh=array();
					$allArTrans=array();
					$arrAllValTime = array();
					$styleTrans = array();
					$cn = 0;
					foreach($raspDay as $transId=>$time){
						$trans = preg_replace("/.*№([0-9ab]+).*/is","$1",$GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$transId]["NAME"], SITE_CHARSET, "utf-8"));
						$allArTrans[$trans][] = $transId;
						foreach($time as $val){
							$allArMarsh[$trans][] = $val;
							$arrAllValTime[$trans][$val] = $transId;
						}
						sort($allArMarsh[$trans],SORT_NUMERIC);
						$allArTrans[$trans] = array_unique($allArTrans[$trans]);
					}
					foreach($allArMarsh as $transId=>$time){
						$cn = 0;
						$arItem = array();
						$arItem["TRANSPORT"] = array();
						
						foreach($allArTrans[$transId] as $keyTrans=>$v){
						if(!isset($styleTrans[$v])) {
							$cn++;
							$styleTrans[$v] = $cn;
						}
						$arItem["TRANSPORT"][] = array(
							"COLOR" => $styleTrans[$v],
							"ID" => $allTransGroup[$arResStation["TRANS"][$v]["ID"]],
							"NAME" => $GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$v]["NAME"], SITE_CHARSET, "utf-8"),
							"MIN_NAME" => preg_replace("/.*№([0-9ab]+).*/is","$1",$GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$v]["NAME"], SITE_CHARSET, "utf-8")),
						);
						}
						
						$arItem["RASP"] = array();
						foreach($time as $val){
						$cnId++;
						$arItem["RASP"][] = array(
							"CNT" => $cnId,
							"TIME_UNIX" => $val,
							"COLOR" => $styleTrans[$arrAllValTime[$transId][$val]],
							"NAME" => $GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$arrAllValTime[$transId][$val]]["NAME"], SITE_CHARSET, "utf-8"),
							"DATE" => date("H:i",(strtotime(date("d.m.Y"))+$val)),
							"TRANS_ID"=>$arResStation["TRANS"][$arrAllValTime[$transId][$val]]["ID"]
						);
						}
						if($typeday == 2){
							$arRes["DAY_4"]["ITEMS"][] = $arItem;
							$arRes["DAY_5"]["ITEMS"][] = $arItem;
						}else{
							$arRes["DAY_".$typeday]["ITEMS"][] = $arItem;
						}
					}
					
				}
			}
			//$arRes["TRANS"] = $GLOBALS["APPLICATION"]->ConvertCharsetArray($arResStation["TRANS"], SITE_CHARSET, "utf-8");
			$arRes["STATION"] = $GLOBALS["APPLICATION"]->ConvertCharsetArray($arResStation["CURRENT_STATION"], SITE_CHARSET, "utf-8");
			$arRes["STATION"]['NAME'] = preg_replace("/(#.*?#)/is","",$arRes["STATION"]['NAME']);
			$arRes["STATION"]['MARSH'] = $arStationsMarsh;
			unset($arResStation);
			
			$arDays = array();
			if(!empty($arRes["DAY_1"]["ITEMS"])) $arDays[] = array('NAME'=>'ПН-ПТ','ID'=>'1','TABID'=>'tab1','TAB_CL'=>'tab-link tab-link-active button');
			//if(!empty($arRes["DAY_2"]["ITEMS"])) $arDays[] = array('NAME'=>'СБ,ВС','ID'=>'2','TABID'=>'tab2','TAB_CL'=>'tab-link button');
			if(!empty($arRes["DAY_4"]["ITEMS"])) $arDays[] = array('NAME'=>'СБ','ID'=>'4','TABID'=>'tab16','TAB_CL'=>'tab-link button');
			if(!empty($arRes["DAY_5"]["ITEMS"])) $arDays[] = array('NAME'=>'ВС','ID'=>'5','TABID'=>'tab15','TAB_CL'=>'tab-link button');
			
			$arAllContent = array(
				"type"=> "station", 
				"text"=>array(
					'day_1' => $arRes["DAY_1"],
					'day_2' => $arRes["DAY_2"],
					'day_4' => $arRes["DAY_4"],
					'day_5' => $arRes["DAY_5"],
					'station' => $arRes["STATION"],
					'days'=>$arDays
				),
				"page"=>$page
			);
			
			foreach($arAllContent['text'] as $day=>$v){
				foreach($v['ITEMS'] as $rasp){
					foreach($rasp['RASP'] as $r){
						$arAllContent['text_all'][$day][] = $r;
					}
				}
			}
			foreach($arAllContent['text_all'] as &$v){
				usort($v, function($a, $b){
					if($a['TIME_UNIX'] === $b['TIME_UNIX'])
						return 0;
						
					return $a['TIME_UNIX'] > $b['TIME_UNIX'] ? 1 : -1;
				});
			}
			unset($v);
			
			//выбор ближайших отправлений
			
			$timeFilter_ = date('H')*60*60 + date('i')*60;
			$timeFilter = $timeFilter_;
			if($timeFilter<10800) $timeFilter = $timeFilter+86400; 
			
			$timeFilter = array($timeFilter-300, $timeFilter+4*60*60);
			
			$TRANS = array();
			if(!empty($userParams->TRANS)) $TRANS = explode(',',$userParams->TRANS);
			
			$filtered = array();
			$not_filteredTime = array();
			$transCount = array();
			foreach($arAllContent['text_all'] as $day=>$r){
				$countRasp = count($r);
				foreach($r as $rasp){
					$mdRasp = md5(self::getMinNameTransport($rasp['NAME']));
					if($rasp['TIME_UNIX']<$timeFilter[1] && $rasp['TIME_UNIX']>$timeFilter[0]) {
						if(!empty($TRANS)){
							if(in_array($rasp['TRANS_ID'],$TRANS)){
								$filtered[$day][] = $rasp;
							}
						}else{
							$filtered[$day][] = $rasp;
						}
					}
					if(!empty($TRANS)){
						if(in_array($rasp['TRANS_ID'],$TRANS)){
							$not_filteredTime[$day][] = $rasp;
							$transCount[$mdRasp] = self::getMinNameTransport($rasp['NAME']);
						}
					}else{
						$not_filteredTime[$day][] = $rasp;
						$transCount[$mdRasp] = self::getMinNameTransport($rasp['NAME']);
					}
					
				}
			}
			//echo'<pre>';print_r($timeFilter);echo'</pre>';
			//echo'<pre>';print_r($arAllContent['text_all']);echo'</pre>';
			
			$dayType = 'day_1';
			$dayText = 'на будние дни';
			if(strpos($text, 'суббот')!==false || strpos($text, 'субот')!==false){
				$dayType = 'day_4';
				$dayText = 'на субботу';
			}
			if(strpos($text, 'воскресен')!==false){
				$dayType = 'day_5';
				$dayText = 'на воскресенье';
			}
			$textReturn = 'Расписание '.$dayText.' на остановке: '." ".$arAllContent['text']['station']['NAME'].", ".$arAllContent['text']['station']['DESC'].'.';
			
			
			
			if(count($transCount)==1){
				$transName = array_values($transCount);
				$textReturnTtx = 'Остановка '.$arAllContent['text']['station']['NAME'].', расписание '.$dayText.', ближайшие отправления маршрута '.$transName[0].': ';
				if(empty($filtered[$dayType])) {
					$textReturnTtx .= "\n"."не найдены.";
				}else{
					$cn = 0;
					foreach($filtered[$dayType] as $r){
						if($r['TIME_UNIX']<$timeFilter_) continue;
						$cn++;
						if($cn>2) break;
						$textReturnTtx .= $r['DATE'].', ';
					}
					if($cn == 0) $textReturnTtx .= "\n"."не найдены.";
				}
				
			}else{
				$textReturnTtx = 'Остановка '.$arAllContent['text']['station']['NAME'].', расписание '.$dayText.', ближайшие отправления: ';
				if(empty($filtered[$dayType])) {
					$textReturnTtx .= "\n"."не найдены.";
				}else{
					$cn = 0;
					foreach($filtered[$dayType] as $r){
						if($r['TIME_UNIX']<$timeFilter_) continue;
						$cn++;
						if($cn>2) break;
						$textReturnTtx .= "\n".$r['DATE'].' - '.self::getMinNameTransport($r['NAME']);
					}
					if($cn == 0) $textReturnTtx .= "\n"."не найдены.";
				}
			
			}
			
			if(count($transCount)==1){
				$transName = array_values($transCount);
				if(empty($not_filteredTime[$dayType])) {
					$textReturn .= "\n"."Не найдено.";
				}else{
					$textReturn .= "\n"."Маршрут: ".$transName[0]."\n";
					foreach($not_filteredTime[$dayType] as $r){
						$textReturn .= $r['DATE'].', ';
					}
				}
			}elseif(count($not_filteredTime[$dayType])<40){
				if(empty($not_filteredTime[$dayType])) {
					$textReturn .= "\n"."Не найдено.";
				}else{
					$cn = 0;
					foreach($not_filteredTime[$dayType] as $r){
						$cn++;
						if($cn>40) break;
						$textReturn .= "\n".$r['DATE'].' - '.self::getMinNameTransport($r['NAME']);
					}
				}
			}else{
			
				if(empty($filtered[$dayType])) {
					$textReturn .= "\n"."Не найдено.";
				}else{
					$cn = 0;
					foreach($filtered[$dayType] as $r){
						$cn++;
						if($cn>30) break;
						$textReturn .= "\n".$r['DATE'].' - '.self::getMinNameTransport($r['NAME']);
					}
				}
			
			}
			
			$buttons = array(
				json_decode('{"hide":"true","title":"ПН-ПТ","payload":"'.$arAllContent['text']['station']['ID'].' на будние дни"}'),
				json_decode('{"hide":"true","title":"СБ","payload":"'.$arAllContent['text']['station']['ID'].' на субботу"}'),
				json_decode('{"hide":"true","title":"ВС","payload":"'.$arAllContent['text']['station']['ID'].' на воскресенье"}'),
			);
			
			return array('TEXT'=>$textReturn, 'TTS'=>$textReturnTtx, 'BUTTONS'=>$buttons);
			
			//echo'<pre>';print_r($textReturn);echo'</pre>';
		
	}
	
	public static function getMinNameTransport($name){
		//$name = self::getUtf8($name);
		$name = preg_replace("/.*№([0-9ab]+).*/is","$1",$name);
		return '№'.$name;
	}
	
	public static function findMarshId($text){
		$text = preg_replace('/([^0-9])/is','',$text);
		if($text=='3') $text = '3a';
		$text = self::toLowerText($text);
		//print_r($text);
		
		$data = array();
		if(!$text) return $data;
		
		$res = \Mlife\Portal\Raspisanie\TransgroupTable::getList(array(
			'select' => array("*"),
			'filter' => array(
				'LOGIC'=>'OR',
				array('NAME'=>'_'.self::getCp1251($text).'_(%'),
				array('NAME'=>'_'.self::getCp1251($text)),
			),
			"order" => array("NAME"=>"ASC")
		));
		while($data = $res->fetch()){
			
			$transIds = array();
			$r = \Mlife\Portal\Raspisanie\TransgroupvaluesTable::getList(
				array(
					"select" => array("*"),
					"filter" => array("ID"=>$data['ID'])
				)
			);
			while($dt = $r->fetch()){
				$transIds[] = $dt['TRANS'];
			}
			
			$data['TRANS_IDS'] = $transIds;
			$data['STATIONS'] = array();
			if(!empty($transIds)){
				$rs = \Mlife\Portal\Raspisanie\TranstostatTable::getList(array(
					'select' => array("SORT","STATION","NAME"=>"ST.NAME"),
					'filter' => array("TRANS"=>$transIds),
					'order' => array("SORT"=>"DESC")
				));
				while($arrs = $rs->fetch()){
					$data['STATIONS'][$arrs["STATION"]] = array($arrs["STATION"], $arrs["NAME"]);
				}
			}
			
			$arItems[] = $data;
		}
		
		$data = self::getUtf8Ar($arItems);
		//echo'<pre>';print_r($data);echo'</pre>';
		return $data;
		
	}
	
	public static function findStationId($text, $stations=false){
			$text = self::toLowerText($text);
			$data = array();
			$text = str_replace(
				array('площадь','улица','деревня','третий','трейтий','четвертый','бульвар','завод','микрорайон','микрораен','переулок','номер'),
				array('пл.','ул.','д.','3-й','3-й','4-й','б-р','з-д','м-н','м-н','пер.','№'),
				$text
			);
			
			//$text = str_replace(' ','%',$text);
			//print_r($text);
			
			$filter = array(
				'LOGIC'=>'AND',
				array('?NAME'=>'%'.self::getCp1251($text).'%'),
				array('!%NAME'=>self::getCp1251('прибытие')),
				array('TYPE'=>3)
			);
			if(!empty($stations)) $filter[0][] = array('=ID'=>$stations);
			
			$res = \Mlife\Portal\Raspisanie\RaspStationTable::getList(array(
				'select'=>array('*'),
				'filter'=>$filter,
				'limit'=>10,
				'order'=>array('ID'=>'ASC')
			));
			$arItems = array();
			while($data = $res->fetch()){
				$arItems[] = $data;
			}
			//print_r($arItems);
			$data = self::getUtf8Ar($arItems);
		return $data;
		
	}
	
	public static function writeToLog($data, $title = ''){
		if (!DEBUG_FILE_NAME)
			return false;

		$log = "\n------------------------\n";
		$log .= date("Y.m.d G:i:s")."\n";
		$log .= (strlen($title) > 0 ? $title : 'DEBUG')."\n";
		$log .= print_r($data, 1);
		$log .= "\n------------------------\n";

		file_put_contents(DEFAULT_DIR."/".DEBUG_FILE_NAME, $log, FILE_APPEND);

		return true;
	}
	
	public static function currentUserData($dialogId,$newParams=false,$noCreateSession=false)
	{
		$cacheId = $dialogId;
		
		if (file_exists(DEFAULT_DIR.'/cache') && file_exists(DEFAULT_DIR.'/cache/'.$cacheId.'.cache') && !$newParams)
		{
			return json_decode(file_get_contents(DEFAULT_DIR.'/cache/'.$cacheId.'.cache'));
		}
		else
		{
			if($noCreateSession) return false;
			if (!file_exists(DEFAULT_DIR.'/cache'))
			{
				mkdir(DEFAULT_DIR.'/cache');
 				chmod(DEFAULT_DIR.'/cache', 0777);
			}
			
			if($newParams){
			$config = json_encode($newParams);
			}else{
			$config = json_encode(array("SESSION"=>$dialogId,"LAST_COMMAND"=>""));
			}
			
			file_put_contents(DEFAULT_DIR.'/cache/'.$cacheId.'.cache', $config);
			chmod(DEFAULT_DIR.'/cache/'.$cacheId.'.cache', 0744);
			return json_decode($config);
		}
	}
	
	public static function getCp1251($text){
		return $GLOBALS["APPLICATION"]->ConvertCharset($text,  "utf-8", SITE_CHARSET);
	}
	public static function getUtf8Ar($data){
		return $GLOBALS["APPLICATION"]->ConvertCharsetArray($data,   SITE_CHARSET, "utf-8");
	}
	public static function getUtf8($data){
		return $GLOBALS["APPLICATION"]->ConvertCharset($data,   SITE_CHARSET, "utf-8");
	}
	
	public static function toLowerText($text){
		return self::getUtf8(toLower(self::getCp1251($text)));
	}
	
	public static function addChatsMessages($res_yandex, $req_bot){
		
		if($res_yandex->request->command == 'ping' || $jsonData->request->payload == 'ping') return;
		
		if(self::$api_type == 'vk'){
			if($req_bot->response->text == 'Завершить диалог.' || strpos($req_bot->response->text, 'Диалог завершен.')!==false) return;
		}
		
		$uid = false;
		
		$userParams = self::currentUserData($res_yandex->session->session_id);
		
		if($userParams->LAST_UID) {
			$uid = $userParams->LAST_UID;
		}else{
		
			$r = \Mlife\Portal\Chatbot\UsersTable::getList(array(
				'select'=>array('ID'),
				'filter'=>array(
					'=SESSION_ID'=>$res_yandex->session->session_id,
					'=USER_KEY'=>$res_yandex->session->user_id,
				),
				'limit'=>1
			))->fetch();
			if($r){
				$uid = $r['ID'];
			}else{
				$rAdd = \Mlife\Portal\Chatbot\UsersTable::add(array(
					'SESSION_ID'=>$res_yandex->session->session_id,
					'USER_KEY'=>$res_yandex->session->user_id,
					'SOURCE'=>'Alisa',
					'DATE_ADD'=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time())
				));
				$uid = $rAdd->getId();
			}
			$userParams->LAST_UID = $uid;
			self::currentUserData($res_yandex->session->session_id,$userParams);
		}
		
		if(!$uid) return;
		
		\Mlife\Portal\Chatbot\MessagesTable::add(array(
			'USER_ID'=>$uid,
			'TYPE'=>'USER',
			'MESS'=>array(
				'TEXT'=>self::getCp1251(($res_yandex->request->command ? $res_yandex->request->command : $res_yandex->request->payload)),
				'TTS'=>'',
				'TYPE'=>$res_yandex->request->type
			),
			'DATE_ADD'=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time())
		));
		
		$btn = array();
		foreach($req_bot->response->buttons as $ob){
			$t = array();
			foreach($ob as $k=>$v){
				$t[$k] = self::getCp1251($v);
			}
			$btn[] = $t;
		}
		\Mlife\Portal\Chatbot\MessagesTable::add(array(
			'USER_ID'=>$uid,
			'TYPE'=>'BOT',
			'MESS'=>array(
				'TEXT'=>self::getCp1251($req_bot->response->text),
				'TTS'=>self::getCp1251($req_bot->response->tts),
				'BUTTONS'=>$btn
			),
			'DATE_ADD'=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time())
		));
	
	}
	
	public static function send($data, $params=array()){
		
		if(self::$api_type == 'vk'){
			
			$json = file_get_contents('php://input');
			$jsonData = json_decode($json);
			
			$url = 'https://api.vk.com/method/messages.send';
			$zapros = array(
				'access_token'=>$params['token'],
				'v'=>'5.38',
				'user_id'=>$jsonData->object->user_id,
				'peer_id'=>'-'.$jsonData->group_id,
				'message'=>$data->response->text
			);
			
			$addButtons = '';
			if(!empty($data->response->buttons)){
				$cn = 0;
				foreach($data->response->buttons as $v){
					if($v->payload) $cn++;
				}
				if($cn>0) $addButtons .= "\n\n".'вы можете отправить команды: ';
				foreach($data->response->buttons as $v){
					if($v->payload) $addButtons .= "\n".'- '.$v->payload.";";
				}
				$zapros['message'] .= $addButtons;
			}
			if($data->response->text == 'Завершить диалог.'){
				$zapros['message'] = 'Диалог завершен.'."\n".'Обращайтесь еще, будем рады помочь.';
				if(file_exists(DEFAULT_DIR.'/cache/'.$data->session->session_id.'.cache')){
					@unlink(DEFAULT_DIR.'/cache/'.$data->session->session_id.'.cache');
				}
			}else{
				$zapros['message'] .= "\n\nДля закрытия диалога с ботом расписаний отправьте команду \"завершить\"";
			}
			
			$httpClient = new \Bitrix\Main\Web\HttpClient();
			$httpClient->post($url, $zapros);
			
			echo 'ok';
			return;
		}
		
		echo json_encode($data);
		return;
	}
	
}