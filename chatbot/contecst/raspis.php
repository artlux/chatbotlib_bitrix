<?
namespace Mlife\Portal\Chatbot\Contecst;

use Mlife\Portal\Chatbot\Convert as Convert;
use Mlife\Portal\Chatbot\Log as Log;

class Raspis implements \Mlife\Portal\Chatbot\Interfaces\Contecst {
	
	public static function getRequestData(){
		return \Mlife\Portal\Chatbot\Main::$data;
	}
	
	public static function getTransportName(){
		return \Mlife\Portal\Chatbot\Main::$transport->getName();
	}
	
	public static function getCommandData($text=''){
		$session = \Mlife\Portal\Chatbot\Main::$user;
		$lastCommand = $session->getParam('LAST_COMMAND');
		$lastData = $session->getParam('LAST_DATA');
		Log::add($text, 'Контекст '.self::class);
		
		$text = Convert::toLowerText($text);
		
		$command = '';
		
		if($text == 'ping') {
			$command = 'ping';
			return $command;
		}elseif($text=='выход') {
			$command = '_exit';
		}elseif($text == '-'){
			$command = '_exit';
		}elseif(strpos($text,'тупица')!==false || strpos($text,'тупая')!==false || strpos($text,'тупой')!==false) {
			$command = '_kompl';
		}elseif(strpos($text,'спасиб')!==false) {
			$command = '_thanks';
		}elseif(strpos($text,'расписание')!==false) {
			$command .= '_rasp';
			
			if(strpos($text,'маршрут')!==false || strpos($text,'автобус')!==false) $command .= '_marsh';
			if(strpos($text,'остановк')!==false) $command .= '_station';
			
		}elseif($lastCommand == '_rasp_station_wait'){
			$command = '_rasp_station_wait';
		}elseif($lastCommand == '_rasp_marsh_wait'){
			$command = '_rasp_marsh_wait';
		}elseif($lastCommand == '_rasp_station' && $lastData){
			$command = '_rasp_station_set';
		}elseif($lastCommand == '_rasp_marsh' && $lastData){
			$command = '_rasp_marsh_set';
		}elseif($lastCommand == '_rasp_start'){
			$command = '_rasp';
		}elseif($lastCommand == '_rasp'){
			
			$command = '_rasp_error';
			
		}
		
		Log::add($text, 'Определение команды '.$command);
		
		return $command;
	}
	
	public static function send($text=''){
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		$sessionId = $session->getSessionId();
		$uid = $session->getParam('LAST_UID');
		$uid_key = $session->getParam('USER_KEY');
		$transportName = self::getTransportName();
		
		if($uid_key){
		
			if(!$uid){
				$r = \Mlife\Portal\Chatbot\UsersTable::getList(array(
					'select'=>array('ID'),
					'filter'=>array(
						'=SESSION_ID'=>$sessionId,
						'=USER_KEY'=>$uid_key,
					),
					'limit'=>1
				))->fetch();
				if($r){
					$uid = $r['ID'];
				}else{
					$rAdd = \Mlife\Portal\Chatbot\UsersTable::add(array(
						'SESSION_ID'=>$sessionId,
						'USER_KEY'=>$uid_key,
						'SOURCE'=>$transportName,
						'DATE_ADD'=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time())
					));
					$uid = $rAdd->getId();
				}
				$session->setParam('LAST_UID',$uid,true);
			}
			
		}
		
		$res = self::getRequestData();
		if($transportName == 'Bitrix24'){
			$type = 'SimpleUtterance';
		}
		
		if($uid){
			
			\Mlife\Portal\Chatbot\MessagesTable::add(array(
				'USER_ID'=>$uid,
				'TYPE'=>'USER',
				'MESS'=>array(
					'TEXT'=>Convert::getCp1251($text),
					'TTS'=>'',
					'TYPE'=>$type
				),
				'DATE_ADD'=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time())
			));
		
		}
		
		$mess = self::presend($text);
		
		$session->setParam('LAST_MESS_DATA',$mess,true);
		
		if($uid && !empty($mess)){
			
			$btn = array();
			if(!empty($mess['buttons'])){
				foreach($mess['buttons'] as $ob){
					$t = array();
					foreach($ob as $k=>$v){
						$t[$k] = Convert::getCp1251($v);
					}
					$btn[] = $t;
				}
			}
			
			\Mlife\Portal\Chatbot\MessagesTable::add(array(
				'USER_ID'=>$uid,
				'TYPE'=>'BOT',
				'MESS'=>array(
					'TEXT'=>Convert::getCp1251($mess['text']),
					'TTS'=>Convert::getCp1251($mess['tts']),
					'BUTTONS'=>$btn
				),
				'DATE_ADD'=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time())
			));
			
		}
		
		return $mess;
		
	}
	
	public static function presend($text=''){
		//print_r($text);
		$session = \Mlife\Portal\Chatbot\Main::$user;
		$command = self::getCommandData($text);
		
		$text = Convert::toLowerText($text);
		
		$mess = array(
			
		);
		
		$data = array();
		
		if($command == 'ping'){
			return false;
		}elseif($command == '_exit'){
			$session->setContecst('Start', true);
			$session->setParam('LAST_COMMAND','', true);
			$session->setParam('LAST_TRANSPORTS','');
			$session->setParam('LAST_DATA','', true);
			
			$mess = array(
				'text' => 'Диалог c ботом завершен.'."\n".'Обращайтесь еще, будем рады помочь.',
				'tts' => 'Обращайтесь еще, будем рады помочь.',
				'buttons' => array()
			);
			$session->setContecst('Start', true);
			
			//$contecstClass = $session->getContecst();
			//return $contecstClass::send($text);
		}elseif($command == '_rasp'){
			$mess = array(
				'text' => 'Примеры запросов на получение расписаний:'."\n".
					'- покажи расписание маршрута 14'."\n".
					'- покажи расписание на остановке площадь ленина'."\n".
					'- покажи расписание маршрута 14 на остановке площадь ленина'."\n\n".
					'также вы можете указать номер маршрута или название остановки'
				,
				'tts' => 'Посмотрите примеры запросов на получение расписаний.',
				'buttons' => array(),
				'buttons_viber'=>self::getMarshButtons()
			);
			$session->setParam('LAST_TRANSPORTS','');
			$session->setParam('LAST_COMMAND','_rasp');
			$session->setParam('LAST_DATA','', true);
		}elseif($command == '_rasp_error'){
			
			$setTempData = false;
			if(strlen(preg_replace('/([^0-9])/','',$text)) == strlen($text)){
				$command = '_rasp_marsh';
				
				$text = 'покажи расписание маршрута '.$text;
				preg_match('/.*?(?:(?:автобус|маршрут).*?\s(.*?)|расписание\s([^\s]+)\s(?:автобус|маршрут).*?)$/i',$text,$data);
				if(count($data)==3){
					$data = array($data[0],($data[2] ? $data[2] : $data[1]));
					
				}
				$session->setParam('LAST_TRANSPORTS','');
				$session->setParam('LAST_DATA','', true);
				$setTempData = true;
			}
			
			if(!$setTempData){
				$temp_res = self::findStationId($text);
				if(count($temp_res)>1){
					$command = '_rasp_station';
					$text = 'покажи расписание на остановке '.$text;
					preg_match('/.*?остановк.*?\s(.*?)$/i',$text,$data);
					$session->setParam('LAST_TRANSPORTS','');
					$session->setParam('LAST_DATA','', true);
					$setTempData = true;
				}
			}
			
			if(!$setTempData){
				
				$lastMess = $session->getParam('LAST_MESS_DATA');
				$buttons_viber = array();
				if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
				
				$session->setParam('LAST_TRANSPORTS','');
				$session->setParam('LAST_COMMAND','_rasp');
				$session->setParam('LAST_DATA','', true);
				
				$mess = \Mlife\Portal\Chatbot\Textbase::noCommandText();
				$mess['buttons'] = array();
				$mess['buttons_viber'] = $buttons_viber;
				return $mess;
			
			}
		}elseif($command == '_thanks'){
			$mess = array(
				'text' => 'Не за что. Обращайтесь, будем рады помочь.',
				'tts' => 'Не за что. Обращайтесь, будем рады помочь.',
				'buttons' => array()
			);
			//$session->setContecst('Start', true);
			//$session->setParam('LAST_COMMAND','', true);
			//$contecstClass = $session->getContecst();
			//return $contecstClass::send($text);
		}elseif($command == '_kompl'){
			
			$lastMess = $session->getParam('LAST_MESS_DATA');
			$buttons_viber = array();
			if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
			
			$mess = array(
				'text' => 'Спасибо за комплимент. А ведь я могу и подкрутить время в расписаниях.',
				'tts' => 'Спасибо за комплимент. А ведь я могу и подкрутить время в расписаниях.',
				'buttons' => array(),
				'buttons_viber'=>$buttons_viber
			);
		}elseif($command == '_rasp_marsh'){
			preg_match('/.*?(?:(?:автобус|маршрут).*?\s(.*?)|расписание\s([^\s]+)\s(?:автобус|маршрут).*?)$/i',$text,$data);
			if(count($data)==3){
				$data = array($data[0],($data[2] ? $data[2] : $data[1]));
			}
			$session->setParam('LAST_TRANSPORTS','');
			$session->setParam('LAST_DATA','', true);
		}elseif($command == '_rasp_station'){
			preg_match('/.*?остановк.*?\s(.*?)$/i',$text,$data);
			$session->setParam('LAST_TRANSPORTS','');
			$session->setParam('LAST_DATA','', true);
		}elseif($command == '_rasp_marsh_station'){
			preg_match('/.*?(?:(?:автобус|маршрут).*?\s([^\s]+)|расписание\s([^\s]+)\s(?:автобус|маршрут).*?\s[на]{2}).*?\sостановк[^\s]+(.*?)$/i',$text,$data);
			if(count($data) == 4){
				$data = array($data[0],($data[1] ? $data[1] : $data[2]),$data[3]);
			}
			$session->setParam('LAST_TRANSPORTS','');
			$session->setParam('LAST_DATA','', true);
		}elseif($command == '_rasp_station_wait'){
			$data = array(
				explode(',',$session->getParam('LAST_DATA'))
			);
		}elseif($command == '_rasp_marsh_wait'){
			$data = array(
				explode(',',$session->getParam('LAST_DATA'))
			);
		}elseif($command == '_rasp_station_set'){
			$data = array(
				$session->getParam('LAST_DATA')
			);
		}elseif($command == '_rasp_marsh_set'){
			$data = array(
				$session->getParam('LAST_DATA')
			);
		}
		
		if(!empty($data)){
			
			Log::add(array($command, $data, $text), 'getContent '.$command);
			$mess = self::getContent($command, $data, $text);
			Log::add($mess, 'Сформированный ответ на команду '.$command);
			return $mess;
			
		}else{
			
			$lastMess = $session->getParam('LAST_MESS_DATA');
			$buttons_viber = array();
			if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
			
			if(!$mess['text']){
				$mess = \Mlife\Portal\Chatbot\Textbase::noCommandText();
				if(empty($mess['buttons'])) $mess['buttons'] = array();
				if(empty($mess['buttons_viber'])) $mess['buttons_viber'] = $buttons_viber;
			}
	
		}
		
		Log::add($mess, 'Сформированный ответ на команду '.$command);
		
		return $mess;
		
	}
	
	public static function getContent($comand, $pAr, $mess=''){
		Log::add($pAr, 'getContent '.$comand);
		$session = \Mlife\Portal\Chatbot\Main::$user;
		if($comand == '_rasp_station') {
			if(count($pAr) == 2){
				$data = self::findStationId($pAr[1]);
				Log::add($data, 'Поиск остановки '.$pAr[1]);
				if(count($data)>1){
					$statIds = array();
					$text = '';
					$buttons_viber = array();
					foreach($data as $cnt=>$station){
						$text .= "\n".$station['ID'].' - '.preg_replace('/\#.*\#/','',$station['NAME'])."\n".$station['DESC'];
						$statIds[] = $station['ID'];
						
						$buttons_viber[] = array(
							'title'=>preg_replace('/\#.*\#/','',$station['NAME'])."\n".$station['DESC'],
							'payload'=>$station['ID']
						);
						
					}
					$session->setParam('LAST_COMMAND','_rasp_station_wait');
					$session->setParam('LAST_DATA',implode(',',$statIds), true);
					return array(
						'text' => "Выберите номер остановки из списка:".$text,
						'tts' => "Выберите номер остановки из списка",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
				}elseif(count($data)==1){
					$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($data[0]['ID']);
					if($res){
						$session->setParam('LAST_COMMAND','_rasp_station');
						$session->setParam('LAST_DATA',$data[0]['ID'], true);
						return self::getStationRasp($res['ID'], $mess);
					}else{
						$session->setParam('LAST_COMMAND','_rasp_station');
						$session->setParam('LAST_DATA','', true);
						
						$lastMess = $session->getParam('LAST_MESS_DATA');
						$buttons_viber = array();
						if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
						
						return array(
							'text' => "Остановка не найдена",
							'tts' => "Остановка не найдена",
							'buttons' => array(),
							'buttons_viber'=>$buttons_viber
						);
					}
				}else{
					$session->setParam('LAST_COMMAND','_rasp_station');
					$session->setParam('LAST_DATA','', true);
					
					$lastMess = $session->getParam('LAST_MESS_DATA');
					$buttons_viber = array();
					if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
					
					return array(
						'text' => "Остановка \"".$pAr[1]."\" не найдена",
						'tts' => "Остановка не найдена",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
				}
				
			}
		}elseif($comand == '_rasp_station_set'){
			
			$setDay = false;
			$mess_fin = $mess;
			$mess_fin = str_replace(
				array('пн','вт', 'ср', 'чт', 'четверг', 'пт', 'пятниц'),
				'на будние',
				$mess_fin
			);
			$mess_fin = str_replace(
				array('сб'),
				'на субботу',
				$mess_fin
			);
			$mess_fin = str_replace(
				array('вс'),
				'на воскресенье',
				$mess_fin
			);
			if(strpos($mess_fin, 'суббот')!==false || strpos($mess_fin, 'субот')!==false){
				$setDay = true;
			}
			if(strpos($mess_fin, 'воскресен')!==false){
				$setDay = true;
			}
			if(strpos($mess_fin, 'выходн')!==false){
				$setDay = true;
			}
			if(strpos($mess_fin, 'будни')!==false){
				$setDay = true;
			}
			
			if($setDay){
				$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($pAr[0]);
				if($res){
					$session->setParam('LAST_COMMAND','_rasp_station');
					$session->setParam('LAST_DATA',$res['ID'], true);
					return self::getStationRasp($res['ID'], $mess);
				}
			}else{
				$lastMess = $session->getParam('LAST_MESS_DATA');
				$buttons_viber = array();
				$buttons = array();
				if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
				if($lastMess['buttons']) $buttons = $lastMess['buttons'];
				
				//Log::add($lastMess, 'debug lastmess');
				
				$mess = \Mlife\Portal\Chatbot\Textbase::noCommandText();
				$mess['buttons'] = array();
				$mess['buttons_viber'] = $buttons_viber;
				return $mess;
			}
		}elseif($comand == '_rasp_station_wait'){
			$id = preg_replace('/([^0-9])/','',$mess);
			$idsAr = $pAr[0];
			if(in_array($id,$idsAr)){
				$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($id);
				if($res){
					$session->setParam('LAST_COMMAND','_rasp_station');
					$session->setParam('LAST_DATA',$res['ID'], true);
					
					$rStat = self::getStationRasp($res['ID'], $mess);
					//$session->setParam('LAST_TRANSPORTS','', true);
					
					return $rStat;
					
				}else{
					$session->setParam('LAST_COMMAND','_rasp_station');
					$session->setParam('LAST_DATA','', true);
					
					$lastMess = $session->getParam('LAST_MESS_DATA');
					$buttons_viber = array();
					if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
					
					return array(
						'text' => "Остановка не найдена",
						'tts' => "Остановка не найдена",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
				}
			}else{
				
				$lastMess = $session->getParam('LAST_MESS_DATA');
				$buttons_viber = array();
				if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
				
				$mess = \Mlife\Portal\Chatbot\Textbase::noCommandText();
				$mess['buttons'] = array();
				$mess['buttons_viber'] = $buttons_viber;
				return $mess;
			}
		}elseif($comand == '_rasp_marsh_wait'){
			$id = preg_replace('/([^0-9])/','',$mess);
			$idsAr = $pAr[0];
			if(in_array($id,$idsAr)){
				$session->setParam('LAST_COMMAND','_rasp_marsh_set');
				$session->setParam('LAST_DATA',$id, true);
				
				$data = self::findMarshId($id, true);
				
				if(count($data)==1){
					$transports = array();
					$statIds = array();
					$text = '';
					$buttons_viber = array();
					foreach($data as $marsh){
						foreach($marsh['TRANS_IDS'] as $trans){
							$transports[$trans] = $trans;
						}
						$text .= "\n\nМаршрут: ".$marsh['NAME'];
						foreach($marsh['STATIONS'] as $station){
							$text .= "\n".$station[0].' - '.preg_replace('/\#.*\#/','',$station[1]);
							$statIds[] = $station[0];
							
							$buttons_viber[] = array(
								'title'=>preg_replace('/\#.*\#/','',$station[1]),
								'payload'=>$station[0]
							);
						}
					}
					
					$session->setParam('LAST_COMMAND','_rasp_station_wait');
					$session->setParam('LAST_TRANSPORTS',implode(',',$transports));
					$session->setParam('LAST_DATA',implode(',',$statIds), true);
				}
				
				return array(
					'text' => "Выберите номер остановки из списка:".$text,
					'tts' => "Выберите номер остановки из списка",
					'buttons' => array(),
					'buttons_viber'=>$buttons_viber
				);
				
			}else{
				
				$lastMess = $session->getParam('LAST_MESS_DATA');
				$buttons_viber = array();
				if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
				
				$mess = \Mlife\Portal\Chatbot\Textbase::noCommandText();
				$mess['buttons'] = array();
				$mess['buttons_viber'] = $buttons_viber;
				return $mess;
			}
		}elseif($comand == '_rasp_marsh'){
			
			if(count($pAr) == 2){
				
				$data = self::findMarshId($pAr[1]);
				
				if(count($data)>1){
					$buttons_viber = array();
					$text = '';
					$transports = array();
					foreach($data as $marsh){
						$text .= "\n".$marsh['ID']." - Маршрут: ".$marsh['NAME'];
						$transports[] = $marsh['ID'];
						$buttons_viber[] = array(
							'title'=>$marsh['NAME'],
							'payload'=>$marsh['ID']
						);
					}
					$session->setParam('LAST_COMMAND','_rasp_marsh_wait');
					$session->setParam('LAST_DATA',implode(',',$transports), true);
					return array(
						'text' => "Выберите направление движения:".$text,
						'tts' => "Выберите направление движения",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
				}elseif(count($data)==1){
					$transports = array();
					$statIds = array();
					$text = '';
					$buttons_viber = array();
					foreach($data as $marsh){
						foreach($marsh['TRANS_IDS'] as $trans){
							$transports[$trans] = $trans;
						}
						$text .= "\n\nМаршрут: ".$marsh['NAME'];
						foreach($marsh['STATIONS'] as $station){
							$text .= "\n".$station[0].' - '.preg_replace('/\#.*\#/','',$station[1]);
							$statIds[] = $station[0];
							
							$buttons_viber[] = array(
								'title'=>preg_replace('/\#.*\#/','',$station[1]),
								'payload'=>$station[0]
							);
							
						}
					}
					
					$session->setParam('LAST_COMMAND','_rasp_station_wait');
					$session->setParam('LAST_TRANSPORTS',implode(',',$transports));
					$session->setParam('LAST_DATA',implode(',',$statIds), true);
					
					return array(
						'text' => "Выберите номер остановки из списка:".$text,
						'tts' => "Выберите номер остановки из списка",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
					
				}else{
					
					$lastMess = $session->getParam('LAST_MESS_DATA');
					$buttons_viber = array();
					if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
					
					//Log::add($lastMess, 'debug lastmess');
					
					return array(
						'text' => "Маршрут №:".$pAr[1]." не найден.",
						'tts' => "Маршрут №:".$pAr[1]." не найден.",
						'buttons' => array(),
						//'buttons_viber'=>$buttons_viber,
						'buttons_viber'=>self::getMarshButtons()
					);
				}
				
			}
			
		}elseif($comand == '_rasp_marsh_station'){
			
			if(count($pAr) == 3){
				
				$data1 = self::findMarshId($pAr[1]);
				$stations = array();
				$transports = array();
				foreach($data1 as $marsh){
					foreach($marsh['TRANS_IDS'] as $trans){
						$transports[$trans] = $trans;
					}
					foreach($marsh['STATIONS'] as $station){
						$stations[] = $station[0];
					}
				}
				
				$session->setParam('LAST_COMMAND','_rasp_marsh_station');
				$session->setParam('LAST_TRANSPORTS',implode(',',$transports));
				$session->setParam('LAST_DATA',implode(',',$stations), true);
				
				$data = self::findStationId($pAr[2], $stations);
				
				if(count($data)>1){
					$text = '';
					$statIds = array();
					$buttons_viber = array();
					foreach($data as $cnt=>$station){
						$text .= "\n".$station['ID'].' - '.preg_replace('/\#.*\#/','',$station['NAME'])."\n".$station['DESC'];
						$statIds[] = $station['ID'];
						
						$buttons_viber[] = array(
							'title'=>preg_replace('/\#.*\#/','',$station['NAME'])."\n".$station['DESC'],
							'payload'=>$station['ID']
						);
						
					}
					$session->setParam('LAST_COMMAND','_rasp_station_wait');
					$session->setParam('LAST_DATA',implode(',',$statIds), true);
					return array(
						'text' => "Выберите номер остановки из списка:".$text,
						'tts' => "Выберите номер остановки из списка",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
				}elseif(count($data)==1){
					
					$res = \Mlife\Portal\Raspisanie\RaspStationTable::getRowById($data[0]['ID']);
					if($res){
						$session->setParam('LAST_COMMAND','_rasp_station');
						$session->setParam('LAST_DATA',$data[0]['ID'], true);
						return self::getStationRasp($res['ID'], $mess);
					}else{
						$session->setParam('LAST_COMMAND','_rasp_station');
						$session->setParam('LAST_DATA','', true);
						
						$lastMess = $session->getParam('LAST_MESS_DATA');
						$buttons_viber = array();
						if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
						
						return array(
							'text' => "Остановка не найдена",
							'tts' => "Остановка не найдена",
							'buttons' => array(),
							'buttons_viber'=>$buttons_viber
						);
					}
					
				}else{
					
					$lastMess = $session->getParam('LAST_MESS_DATA');
					$buttons_viber = array();
					if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
					
					return array(
						'text' => "Остановка \"".$pAr[2]."\" на маршруте \"".$pAr[1]."\" не найдена",
						'tts' => "Остановка \"".$pAr[2]."\" на маршруте \"".$pAr[1]."\" не найдена",
						'buttons' => array(),
						'buttons_viber'=>$buttons_viber
					);
				}
				
			}
			
		}
		
		$lastMess = $session->getParam('LAST_MESS_DATA');
		$buttons_viber = array();
		if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
		
		$mess = \Mlife\Portal\Chatbot\Textbase::noCommandText();
		$mess['buttons'] = array();
		$mess['buttons_viber'] = $buttons_viber;
		return $mess;
		
	}
	
	public static function findStationId($text, $stations=false){
			$text = Convert::toLowerText($text);
			$data = array();
			$text = str_replace(
				array('площадь','улица','деревня','третий','трейтий','четвертый','бульвар','завод','микрорайон','микрораен','переулок','номер'),
				array('пл.','ул.','д.','3-й','3-й','4-й','б-р','з-д','м-н','м-н','пер.','№'),
				$text
			);
			
			//$text = str_replace(' ','%',$text);
			//print_r($text);
			
			$filter = array(
				'LOGIC'=>'AND',
				array('?NAME'=>'%'.Convert::getCp1251($text).'%'),
				array('!%NAME'=>Convert::getCp1251('прибытие')),
				array('TYPE'=>3)
			);
			if(!empty($stations)) $filter[0][] = array('=ID'=>$stations);
			
			$res = \Mlife\Portal\Raspisanie\RaspStationTable::getList(array(
				'select'=>array('*'),
				'filter'=>$filter,
				'limit'=>10,
				'order'=>array('ID'=>'ASC')
			));
			$arItems = array();
			while($data = $res->fetch()){
				$arItems[] = $data;
			}
			//Log::add($arItems, 'Поиск остановки '.$text);
			//print_r($arItems);
			$data = Convert::getUtf8Ar($arItems);
		return $data;
		
	}
	
	public static function getStationRasp($stationId, $text=''){
		
		$userParams = \Mlife\Portal\Chatbot\Main::$user;
		
		$allTransGroup = array();
		if(empty($allTransGroup)){
		$r2 = \Mlife\Portal\Raspisanie\TransgroupvaluesTable::getList(
			array(
				"select"=>array("TRANS","ID"),
			)
		);
		while($data = $r2->fetch()){
			$allTransGroup[$data["TRANS"]] = $data["ID"];
		}
		}
		
		//$stationId = str_replace("station_","",$page);
		
		
		
		$filter = array("TR.TIP"=>3, "STATION"=> $stationId);
		//if(!empty($userParams->getParam('TRANS'))) $filter['TRANS'] = explode(',',$userParams->getParam('TRANS'));
		
		//список маршрутов по станции
		$res = \Mlife\Portal\Raspisanie\TranstostatTable::getList(array(
			'select' => array("STATION","TRANS","NAME"=>"TR.NAME"),
			'runtime' => array(
				new \Bitrix\Main\Entity\ReferenceField('TR', '\Mlife\Portal\Raspisanie\TransportTable', 
					array('=this.TRANS' => 'ref.ID'), array("join_type"=>"left")
				),
			),
			'filter' => $filter
		));
		$arStationsMarsh = array();
		while($data = $res->fetch()){
			$num = preg_replace("/.*№([0-9aа]+).*?$/is","$1",$GLOBALS["APPLICATION"]->ConvertCharset($data["NAME"], SITE_CHARSET, "utf-8"));
			if($num) $arStationsMarsh[$num] = $num;
		}
		asort($arStationsMarsh,SORT_NUMERIC);
		
		//получение описания станций
		$res = \Mlife\Portal\Raspisanie\RaspStationTable::getById($stationId);
		$arResStation["CURRENT_STATION"] = $res->Fetch();
		
		$arResStation["ID"] = $arResStation["CURRENT_STATION"]["ID"];
		
		//получение маршрутов по данной станции
		$res = \Mlife\Portal\Raspisanie\TransportTable::getList(array(
			"select" => array("*"),
			"filter" => array("S_TO.STATION"=>$arResStation["CURRENT_STATION"]["ID"], "TIP"=>3),
			'runtime' => array(
				new \Bitrix\Main\Entity\ReferenceField('S_TO', '\Mlife\Portal\Raspisanie\TranstostatTable', 
					array('=this.ID' => 'ref.TRANS')
				),
			),
		));
		$arResStation["TRANS"] = array();
		while($arData = $res->fetch()){
			$arResStation["TRANS"][$arData["ID"]] = $arData;			
		}
		
		//получаем расписания
		$res = \Mlife\Portal\Raspisanie\RaspdatetimeTable::getList(array(
			"filter" => array(
				"STATION"=>$arResStation["CURRENT_STATION"]["ID"], 
				">S_TO.DATE_TO"=>\Bitrix\Main\Type\DateTime::createFromTimestamp(time()),
				"S_TO.TRANS"=> array_keys($arResStation["TRANS"])
			),
			"runtime" => array(
				new \Bitrix\Main\Entity\ReferenceField('S_TO', '\Mlife\Portal\Raspisanie\RaspdateTable', 
					array('=this.RASP' => 'ref.ID')
				),
			),
			"select" => array("TO_"=>"S_TO.*","*"),
			"order" => array("TO_TIME_OTP"=>"ASC")
		));
		$arResStation["RASP"] = array();
		while($arData = $res->fetch()){
			$date = $arData["TO_TIME_OTP"]+($arData["TIME_PRIB"]*60);
			$arResStation["RASP"][$arData["TO_DAY"]][$arData["TO_TRANS"]][] = $date;
		}
		$arRes = array(
		'DAY_1'=>array('ITEMS'=>array()), 
		'DAY_4'=>array('ITEMS'=>array()), 
		'DAY_5'=>array('ITEMS'=>array()), 
		'DAY_2'=>array('ITEMS'=>array())
		);
		//echo '<pre>';print_r($arResStation["RASP"]);echo'</pre>';
		foreach($arResStation["RASP"] as $typeday=>$raspDay){
			if($typeday==1 || $typeday==2 || $typeday==4 || $typeday==5){
				//сортируем по количеству рейсов
				$newRasp = array();
				$raspDayTemp = array();
				foreach($raspDay as $transId=>$time){
					$newRasp[$transId] = count($time);
				}
				arsort($newRasp,SORT_NUMERIC);
				foreach($newRasp as $vv=>$tt){
					$raspDayTemp[$vv] = $raspDay[$vv];
				}
				$raspDay = $raspDayTemp;
				unset($raspDayTemp);
				unset($newRasp);
				
				$allArMarsh=array();
				$allArTrans=array();
				$arrAllValTime = array();
				$styleTrans = array();
				$cn = 0;
				foreach($raspDay as $transId=>$time){
					$trans = preg_replace("/.*№([0-9ab]+).*/is","$1",$GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$transId]["NAME"], SITE_CHARSET, "utf-8"));
					$allArTrans[$trans][] = $transId;
					foreach($time as $val){
						$allArMarsh[$trans][] = $val;
						$arrAllValTime[$trans][$val] = $transId;
					}
					sort($allArMarsh[$trans],SORT_NUMERIC);
					$allArTrans[$trans] = array_unique($allArTrans[$trans]);
				}
				foreach($allArMarsh as $transId=>$time){
					$cn = 0;
					$arItem = array();
					$arItem["TRANSPORT"] = array();
					
					foreach($allArTrans[$transId] as $keyTrans=>$v){
					if(!isset($styleTrans[$v])) {
						$cn++;
						$styleTrans[$v] = $cn;
					}
					$arItem["TRANSPORT"][] = array(
						"COLOR" => $styleTrans[$v],
						"ID" => $allTransGroup[$arResStation["TRANS"][$v]["ID"]],
						"NAME" => $GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$v]["NAME"], SITE_CHARSET, "utf-8"),
						"MIN_NAME" => preg_replace("/.*№([0-9ab]+).*/is","$1",$GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$v]["NAME"], SITE_CHARSET, "utf-8")),
					);
					}
					
					$arItem["RASP"] = array();
					foreach($time as $val){
					$cnId++;
					$arItem["RASP"][] = array(
						"CNT" => $cnId,
						"TIME_UNIX" => $val,
						"COLOR" => $styleTrans[$arrAllValTime[$transId][$val]],
						"NAME" => $GLOBALS["APPLICATION"]->ConvertCharset($arResStation["TRANS"][$arrAllValTime[$transId][$val]]["NAME"], SITE_CHARSET, "utf-8"),
						"DATE" => date("H:i",(strtotime(date("d.m.Y"))+$val)),
						"TRANS_ID"=>$arResStation["TRANS"][$arrAllValTime[$transId][$val]]["ID"]
					);
					}
					if($typeday == 2){
						$arRes["DAY_4"]["ITEMS"][] = $arItem;
						$arRes["DAY_5"]["ITEMS"][] = $arItem;
					}else{
						$arRes["DAY_".$typeday]["ITEMS"][] = $arItem;
					}
				}
				
			}
		}
		//$arRes["TRANS"] = $GLOBALS["APPLICATION"]->ConvertCharsetArray($arResStation["TRANS"], SITE_CHARSET, "utf-8");
		$arRes["STATION"] = $GLOBALS["APPLICATION"]->ConvertCharsetArray($arResStation["CURRENT_STATION"], SITE_CHARSET, "utf-8");
		$arRes["STATION"]['NAME'] = preg_replace("/(#.*?#)/is","",$arRes["STATION"]['NAME']);
		$arRes["STATION"]['MARSH'] = $arStationsMarsh;
		unset($arResStation);
		
		$arDays = array();
		if(!empty($arRes["DAY_1"]["ITEMS"])) $arDays[] = array('NAME'=>'ПН-ПТ','ID'=>'1','TABID'=>'tab1','TAB_CL'=>'tab-link tab-link-active button');
		//if(!empty($arRes["DAY_2"]["ITEMS"])) $arDays[] = array('NAME'=>'СБ,ВС','ID'=>'2','TABID'=>'tab2','TAB_CL'=>'tab-link button');
		if(!empty($arRes["DAY_4"]["ITEMS"])) $arDays[] = array('NAME'=>'СБ','ID'=>'4','TABID'=>'tab16','TAB_CL'=>'tab-link button');
		if(!empty($arRes["DAY_5"]["ITEMS"])) $arDays[] = array('NAME'=>'ВС','ID'=>'5','TABID'=>'tab15','TAB_CL'=>'tab-link button');
		
		$arAllContent = array(
			"type"=> "station", 
			"text"=>array(
				'day_1' => $arRes["DAY_1"],
				'day_2' => $arRes["DAY_2"],
				'day_4' => $arRes["DAY_4"],
				'day_5' => $arRes["DAY_5"],
				'station' => $arRes["STATION"],
				'days'=>$arDays
			),
			"page"=>$page
		);
		
		foreach($arAllContent['text'] as $day=>$v){
			foreach($v['ITEMS'] as $rasp){
				foreach($rasp['RASP'] as $r){
					$arAllContent['text_all'][$day][] = $r;
				}
			}
		}
		foreach($arAllContent['text_all'] as &$v){
			usort($v, function($a, $b){
				if($a['TIME_UNIX'] === $b['TIME_UNIX'])
					return 0;
					
				return $a['TIME_UNIX'] > $b['TIME_UNIX'] ? 1 : -1;
			});
		}
		unset($v);
		
		//выбор ближайших отправлений
		
		$timeFilter_ = date('H')*60*60 + date('i')*60;
		$timeFilter = $timeFilter_;
		if($timeFilter<10800) $timeFilter = $timeFilter+86400; 
		
		$timeFilter = array($timeFilter-300, $timeFilter+4*60*60);
		
		$TRANS = array();
		if(!empty($userParams->getParam('LAST_TRANSPORTS'))) $TRANS = explode(',',$userParams->getParam('LAST_TRANSPORTS'));
		
		$filtered = array();
		$not_filteredTime = array();
		$transCount = array();
		foreach($arAllContent['text_all'] as $day=>$r){
			$countRasp = count($r);
			foreach($r as $rasp){
				$mdRasp = md5(self::getMinNameTransport($rasp['NAME']));
				if($rasp['TIME_UNIX']<$timeFilter[1] && $rasp['TIME_UNIX']>$timeFilter[0]) {
					if(!empty($TRANS)){
						if(in_array($rasp['TRANS_ID'],$TRANS)){
							$filtered[$day][] = $rasp;
						}
					}else{
						$filtered[$day][] = $rasp;
					}
				}
				if(!empty($TRANS)){
					if(in_array($rasp['TRANS_ID'],$TRANS)){
						$not_filteredTime[$day][] = $rasp;
						$transCount[$mdRasp] = self::getMinNameTransport($rasp['NAME']);
					}
				}else{
					$not_filteredTime[$day][] = $rasp;
					$transCount[$mdRasp] = self::getMinNameTransport($rasp['NAME']);
				}
				
			}
		}
		//echo'<pre>';print_r($timeFilter);echo'</pre>';
		//echo'<pre>';print_r($arAllContent['text_all']);echo'</pre>';
		
		$dayType = 'day_1';
		$dayText = 'на будние дни';
		if(strpos($text, 'суббот')!==false || strpos($text, 'субот')!==false){
			$dayType = 'day_4';
			$dayText = 'на субботу';
		}
		if(strpos($text, 'воскресен')!==false){
			$dayType = 'day_5';
			$dayText = 'на воскресенье';
		}
		if(strpos($text, 'выходн')!==false){
			$dayType = 'day_5';
			$dayText = 'на воскресенье';
		}
		$textReturn = 'Расписание '.$dayText.' на остановке: '." ".$arAllContent['text']['station']['NAME'].", ".$arAllContent['text']['station']['DESC'].'.';
		
		
		
		if(count($transCount)==1){
			$transName = array_values($transCount);
			$textReturnTtx = 'Остановка '.$arAllContent['text']['station']['NAME'].', расписание '.$dayText.', ближайшие отправления маршрута '.$transName[0].': ';
			if(empty($filtered[$dayType])) {
				$textReturnTtx .= "\n"."не найдены.";
			}else{
				$cn = 0;
				foreach($filtered[$dayType] as $r){
					if($r['TIME_UNIX']<$timeFilter_) continue;
					$cn++;
					if($cn>2) break;
					$textReturnTtx .= $r['DATE'].', ';
				}
				if($cn == 0) $textReturnTtx .= "\n"."не найдены.";
			}
			
		}else{
			$textReturnTtx = 'Остановка '.$arAllContent['text']['station']['NAME'].', расписание '.$dayText.', ближайшие отправления: ';
			if(empty($filtered[$dayType])) {
				$textReturnTtx .= "\n"."не найдены.";
			}else{
				$cn = 0;
				foreach($filtered[$dayType] as $r){
					if($r['TIME_UNIX']<$timeFilter_) continue;
					$cn++;
					if($cn>2) break;
					$textReturnTtx .= "\n".$r['DATE'].' - '.self::getMinNameTransport($r['NAME']);
				}
				if($cn == 0) $textReturnTtx .= "\n"."не найдены.";
			}
		
		}
		
		if(count($transCount)==1){
			$transName = array_values($transCount);
			if(empty($not_filteredTime[$dayType])) {
				$textReturn .= "\n"."Не найдено.";
			}else{
				$textReturn .= "\n"."Маршрут: ".$transName[0]."\n";
				foreach($not_filteredTime[$dayType] as $r){
					$textReturn .= $r['DATE'].', ';
				}
			}
		}elseif(count($not_filteredTime[$dayType])<40){
			if(empty($not_filteredTime[$dayType])) {
				$textReturn .= "\n"."Не найдено.";
			}else{
				$cn = 0;
				foreach($not_filteredTime[$dayType] as $r){
					$cn++;
					if($cn>40) break;
					$textReturn .= "\n".$r['DATE'].' - '.self::getMinNameTransport($r['NAME']);
				}
			}
		}else{
		
			if(empty($filtered[$dayType])) {
				$textReturn .= "\n"."Не найдено.";
			}else{
				$cn = 0;
				foreach($filtered[$dayType] as $r){
					$cn++;
					if($cn>30) break;
					$textReturn .= "\n".$r['DATE'].' - '.self::getMinNameTransport($r['NAME']);
				}
			}
		
		}
		
		$buttons = array(
			array(
				'hide' => true,
				'title_button' => "ПН-ПТ",
				'title_comand' => "на будние",
				'payload' => "на будние"
			),
			array(
				'hide' => true,
				'title_button' => "СБ",
				'title_comand' => "на субботу",
				'payload' => "на субботу"
			),
			array(
				'hide' => true,
				'title_button' => "ВС",
				'title_comand' => "на воскресенье",
				'payload' => "на воскресенье"
			),
		);
		
		return array('text'=>$textReturn, 'tts'=>$textReturnTtx, 'buttons'=>$buttons);
		
		//echo'<pre>';print_r($textReturn);echo'</pre>';
	
	}
	
	public static function findMarshId($text,$groupId=false){
		
		if(!$groupId){
		
			$text = preg_replace('/([^0-9])/is','',$text);
			if($text=='3') $text = '3a';
			$text = Convert::toLowerText($text);
			//print_r($text);
		
		}
		
		$data = array();
		if(!$text) return $data;
		
		$fl = array(
			'LOGIC'=>'OR',
			array('NAME'=>'_'.Convert::getCp1251($text).'_(%'),
			array('NAME'=>'_'.Convert::getCp1251($text)),
		);
		
		if($groupId) $fl = array('ID'=>$text);
		
		$res = \Mlife\Portal\Raspisanie\TransgroupTable::getList(array(
			'select' => array("*"),
			'filter' => $fl,
			"order" => array("NAME"=>"ASC")
		));
		while($data = $res->fetch()){
			
			$transIds = array();
			$r = \Mlife\Portal\Raspisanie\TransgroupvaluesTable::getList(
				array(
					"select" => array("*"),
					"filter" => array("ID"=>$data['ID'])
				)
			);
			while($dt = $r->fetch()){
				$transIds[] = $dt['TRANS'];
			}
			
			$data['TRANS_IDS'] = $transIds;
			$data['STATIONS'] = array();
			if(!empty($transIds)){
				$rs = \Mlife\Portal\Raspisanie\TranstostatTable::getList(array(
					'select' => array("SORT","STATION","NAME"=>"ST.NAME"),
					'filter' => array("TRANS"=>$transIds),
					'order' => array("SORT"=>"DESC")
				));
				while($arrs = $rs->fetch()){
					$data['STATIONS'][$arrs["STATION"]] = array($arrs["STATION"], $arrs["NAME"]);
				}
			}
			
			$arItems[] = $data;
		}
		
		$data = Convert::getUtf8Ar($arItems);
		//echo'<pre>';print_r($data);echo'</pre>';
		return $data;
		
	}
	
	public static function getMarshButtons(){
		$arButtons = array();
		$res = \Mlife\Portal\Raspisanie\TransgroupTable::getList(array(
			'select' => array("NAME"),
			'filter' => array(),
			"order" => array("NAME"=>"ASC")
		));
		while($dt = $res->fetch()){
			$name = str_replace('№','',self::getMinNameTransport(Convert::getUtf8($dt['NAME'])));
			$arButtons[md5($name)] = array(
				'title'=>'№'.$name,
				'payload'=>preg_replace('/([^0-9])/','',$name),
				'columns'=>2
			);
		}
		return $arButtons;
	}
	
	public static function getMinNameTransport($name){
		//$name = self::getUtf8($name);
		$name = preg_replace("/.*№([0-9ab]+).*/is","$1",$name);
		return '№'.$name;
	}
	
}