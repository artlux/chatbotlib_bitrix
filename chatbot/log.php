<?
namespace Mlife\Portal\Chatbot;

class Log {
	
	public static function add($data, $title = ''){
		if (!DEBUG_FILE_NAME)
			return false;
			
		if (!DEFAULT_DIR)
			return false;

		$log = "\n------------------------\n";
		$log .= date("Y.m.d G:i:s")."\n";
		$log .= (strlen($title) > 0 ? $title : 'DEBUG')."\n";
		$log .= print_r($data, 1);
		$log .= "\n------------------------\n";

		file_put_contents(DEFAULT_DIR."/".DEBUG_FILE_NAME, $log, FILE_APPEND);

		return true;
	}
	
}