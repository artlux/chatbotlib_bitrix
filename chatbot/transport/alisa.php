<?
namespace Mlife\Portal\Chatbot\Transport;

use Mlife\Portal\Chatbot\Log as Log;

class Alisa{
	
	public $config = array();
	public $appsConfig = array();
	public $cacheData = array();
	
	function __construct($config = array()) {
		
		$this->config = $config;
		
		try{
			$json = file_get_contents('php://input');

			$jsonData = json_decode($json);
			
			$this->cacheData = $jsonData;
		
		}catch(\Exception $ex){
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			echo $ex->getMessage();
		}
		
	}
	
	public static function getName(){
		return static::class;
	}
	
	public function getConfigSession(){
		
		if(!isset($this->cacheData->session->session_id)) return array();
		
		return array(
			'SESSION' => array(
				'ID'=>$this->cacheData->session->session_id,
			)
		);
	}
	
	public function getConfigUser(){
		
		if(!isset($this->cacheData->session->user_id)) return array();
		
		return array(
			'USER_KEY'=>'alisa_'.$this->cacheData->session->user_id
		);
	}
	
	public function request($data){
		
		if(empty($data)) $data = $this->cacheData;
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session === null) return $this->sendMessage(false);
		
		if($data->request->payload && !$data->request->command) {
			$data->request->command = $data->request->payload;
			$this->cacheData->request->command = $data->request->payload;
		}
		if($this->cacheData->session->new){
			$this->cacheData->request->command = '/start';
			$data->request->command = '/start';
		}
		
		if($data->request->command){
			
			//подставляем payload для значения текста в кнопке
			$lastMess = $session->getParam('LAST_MESS_DATA');
			$buttons_viber = array();
			$buttons = array();
			if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
			if($lastMess['buttons']) $buttons = $lastMess['buttons'];
			
			$buttons_viber[] = array(
				"title"=> 'маршруты',
				"payload"=>'покажи расписание'
			);
			$buttons_viber[] = array(
				"title"=> 'выход в меню',
				"payload"=>'выход'
			);
			
			Log::add($lastMess, 'ImBot lastMess');
			
			foreach($buttons_viber as $b){
				if(trim($b['title']) == trim($data->request->command)) {
					if($b['payload']) {
						$data->request->command = $b['payload'];
						break;
					}
				}
			}
			
			foreach($buttons as $b){
				if(trim($b['title_button']) == trim($data->request->command)) {
					if($b['payload']) {
						$data->request->command = $b['payload'];
						break;
					}
				}
			}
			
			$this->cacheData->request->command = $data->request->command;
			
			$mess = trim($data->request->command);
			
			$contecstClass = $session->getContecst();
			
			if(!$session->getParam('CONTECST')) {
				$session->setContecst('Start', true);
				$contecstClass = $session->getContecst();
			}
			
			if($arMess = \Mlife\Portal\Chatbot\Convert::getGlobalCommand($mess)) {
				$mess = $arMess[0];
				$session->setContecst($arMess[1], true);
				$contecstClass = $session->getContecst();
			}
			
			Log::add($data, 'ImBot Event Query');
			$mess = $contecstClass::send($mess);
			
			if(empty($mess) && $session->getParam('CONTECST') == 'Operator'){
				echo \Mlife\Portal\Chatbot\Convert::resendFromViber();
				return;
			}elseif($session->getParam('CONTECST') == 'Operator'){
				\Mlife\Portal\Chatbot\Convert::resendFromViber();
			}
			
			$result = $this->sendMessage($mess);
		
		}
		
		return $this->sendMessage(false);
		
	}
	
	public function sendMessage($mess){
		
		if($mess === false) {
			//echo 'ok';
			return false;
		}
		
		if(!$mess['text']) {
			Log::add($mess, 'message is empty');
			return false;
		}
		
		$mess = $this->prepareData($mess);
		
		/*$data = array(
			'response'=>array(
				'end_session'=>false,
				'text'=>$mess['text'],
				'tts'=>$mess['tts']
			),
			'session'=>array(
				'session_id'=>$this->cacheData->session->session_id,
				'message_id'=>$this->cacheData->session->message_id,
				'user_id'=>$this->cacheData->session->user_id
			),
			'version'=>'1.0'
		);*/
		
		//echo json_encode($data);
		
		$data = new \stdClass();
		$data->response = new \stdClass();
		$data->session = new \stdClass();
		$data->version = "1.0";
		
		$data->response->end_session = false;
		$data->session->session_id = $this->cacheData->session->session_id;
		$data->session->message_id = $this->cacheData->session->message_id;
		$data->session->user_id = $this->cacheData->session->user_id;
		
		$data->response->text = $mess['text'];
		$data->response->tts = $mess['tts'];
		
		$keyBoard = array();
		if(!empty($mess['keyboard']['keyboard'])){
			
			foreach($mess['keyboard']['keyboard'] as $k=>$v){
				
				$but = new \stdClass();
				
				$but->title = $v['text'];
				$but->hide = true;
				$but->payload = $v['callback_data'];
				
				$keyBoard[] = $but;
				
			}
			
		}else{
			$session = \Mlife\Portal\Chatbot\Main::$user;
			if($session->getParam('LAST_COMMAND') == 'menu'){
				
			}else{
			
				$but = new \stdClass();
					
				$but->title = 'выход в меню';
				$but->hide = true;
				$but->payload = 'выход в меню';
				
				$keyBoard[] = $but;
			
			}
			
		}
		
		if(!empty($keyBoard)){
			$data->response->buttons = $keyBoard;
		}else{
			//$data->response->buttons = array();
		}
		
		$r = json_encode($data);
		Log::add($data, 'data for alisa');
		echo $r;
		
		return true;
		
	}
	
	private function prepareData($data){
		
		$mess = $data['text'];
		if(!$mess) return $data;
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session->getParam('CONTECST') == 'Raspis'){
		
			$data['keyboard'] = array(
				"one_time_keyboard" => true,
				//"remove_keyboard" => true,
				//"selective"=> true,
				"resize_keyboard" => true,
				"keyboard"=>array(
					array(
						"text"=> 'маршруты',
						"callback_data"=>'покажи расписание',
						"column"=>3,
					),
					array(
						"text"=> 'выход',
						"callback_data"=>'выход',
						"column"=>3,
					)
				)
			);
			
			if(!empty($data['buttons_marsh'])){
				
				foreach($data['buttons_marsh'] as $b){
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload']
					);
				}
			}
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['keyboard'])>40) break;
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload'],
						"column"=>$b['columns'],
					);
				}
			}
			
			if(!empty($data['buttons'])){
				$col = 6;
				if(strlen($data['buttons'][0]['title_button'])<5) $col = 2;
				foreach($data['buttons'] as $v){
					$data['keyboard']['keyboard'][] = array(
						"text"=> $v['title_button'],
						"callback_data"=>$v['payload'],
						"column"=>$col,
					);
				}
				/*$cm = array();
				foreach($data['buttons'] as $v){
					$cm[] = $v['title_comand'];
				}
				$mess .= "\n\nДоступны команды: ".implode('; ',$cm).".";
				*/
			}
			
		}elseif($session->getParam('CONTECST') == 'Reklama'){
				
			$data['keyboard'] = array(
				"one_time_keyboard" => true,
				//"remove_keyboard" => true,
				//"selective"=> true,
				"resize_keyboard" => true,
			);
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['keyboard'])>40) break;
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload'],
						"column"=>$b['columns'],
					);
				}
			}
			
		}elseif($session->getParam('CONTECST') == 'StartAlice'){
				
			$data['keyboard'] = array(
				"one_time_keyboard" => true,
				//"remove_keyboard" => true,
				//"selective"=> true,
				"resize_keyboard" => true,
			);
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['keyboard'])>40) break;
					$data['keyboard']['keyboard'][] = array(
						"text"=> $b['title'],
						"callback_data"=>$b['payload'],
						"column"=>$b['columns'],
					);
				}
			}
			
		}
		
		if(isset($data['keyboard']) && empty($data['keyboard']['keyboard'])) unset($data['keyboard']);
		
		if($session->getParam('CONTECST') == 'Raspis'){
			$mess .= "\n\nДля завершения диалога отправьте команду: выход.";
		}
		
		$data['text'] = $mess;
		
		return $data;
		
	}
	
	//обработчик на замену контекста
	public static function OnSetContecst(\Bitrix\Main\Event $event){
		$params = $event->getParameters();
		$contecst = $params['CONTECST'];
		if($contecst == 'Start') {
			$contecst = 'StartAlice';
			$result = new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::SUCCESS,array('CONTECST'=>$contecst));
			return $result;
		}
	}
}