<?
namespace Mlife\Portal\Chatbot;

class Usersession {
	
	protected $params = null;
	protected $sessionId = null;
	protected $sessionLocked = false;
	
	//$expired - время жизни в секундах
	function __construct($sessionId, $defaultParams = array(), $expired=false) {
		
		if(!isset($defaultParams['CONTECST'])){
			$defaultParams['CONTECST'] = 'Start';
		}
		
		$this->sessionId = $sessionId;
		
		$cacheId = $this->sessionId;
		
		if (!DEFAULT_DIR)
			return false;
		
		$fileName = DEFAULT_DIR.'/cache/'.$cacheId.'.cache';
		if (file_exists($fileName)){
			
			//$this->params = \Bitrix\Main\Web\Json::decode(file_get_contents(DEFAULT_DIR.'/cache/'.$cacheId.'.cache'));
			$this->params = unserialize(file_get_contents(DEFAULT_DIR.'/cache/'.$cacheId.'.cache'));
			
			if($this->params['SESSION_EXPIRED']) $expired = $this->params['SESSION_EXPIRED'];
			
			if($expired){
				
				$timeFile = filemtime($fileName) + $expired;
				if($timeFile < time()){
					$this->params = $defaultParams;
					if($defaultParams['CONTECST']) $this->setContecst($defaultParams['CONTECST']);
					$this->save();
				}
				
			}else{
			
				//$this->params = \Bitrix\Main\Web\Json::decode(file_get_contents(DEFAULT_DIR.'/cache/'.$cacheId.'.cache'));
				$this->params = unserialize(file_get_contents(DEFAULT_DIR.'/cache/'.$cacheId.'.cache'));
			
			}
		}else{
			$this->params = $defaultParams;
			if($defaultParams['CONTECST']) $this->setContecst($defaultParams['CONTECST']);
		}
		
	}
	
	public function getSessionId(){
		return $this->sessionId;
	}
	
	public function setContecst($contecst, $replace=false){
		
		$event = new \Bitrix\Main\Event("mlife.portal", "OnSetContecst",array('CONTECST'=>$contecst));
		$event->send();
		if ($event->getResults()){
			foreach($event->getResults() as $evenResult){
				if($evenResult->getResultType() == \Bitrix\Main\EventResult::SUCCESS){
					$params = $evenResult->getParameters();
					if($params['CONTECST']) $contecst = $params['CONTECST'];
				}
			}
		}
		//\Mlife\Portal\Chatbot\Log::add($contecst,'CONTECST set');
		$this->params['CONTECST'] = $contecst;
		if($replace) $this->save();
	}
	public function getContecst(){
		return '\\Mlife\\Portal\\Chatbot\\Contecst\\'.$this->params['CONTECST'];
	}
	
	public function setParam($name, $value='', $replace=false){
		$this->params[$name] = $value;
		//\Mlife\Portal\Chatbot\Log::add($value,'setParam '.$name);
		if($replace) $this->save();
	}
	
	public function getParam($name){
		if(isset($this->params[$name]))
			return $this->params[$name];
		return false;
	}
	
	public function save(){
		if (!DEFAULT_DIR)
			return false;
		
		if($this->sessionLocked) return false;
		
		//$config = \Bitrix\Main\Web\Json::encode($this->params);
		$config = serialize($this->params);
		//\Mlife\Portal\Chatbot\Log::add($config,'set Session');
		file_put_contents(DEFAULT_DIR.'/cache/'.$this->sessionId.'.cache', $config);
		chmod(DEFAULT_DIR.'/cache/'.$this->sessionId.'.cache', 0744);
	}
	
	public function lock(){
		$this->sessionLocked = true;
	}
	
	public function unlock(){
		$this->sessionLocked = false;
	}
	
	public function delete(){
		if (!DEFAULT_DIR)
			return false;
		
		if(file_exists(DEFAULT_DIR.'/cache/'.$this->sessionId.'.cache')){
			@unlink(DEFAULT_DIR.'/cache/'.$this->sessionId.'.cache');
		}
		
		$this->params = null;
		$this->sessionId = null;
	}
	
}