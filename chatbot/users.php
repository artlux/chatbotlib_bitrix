<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.portal
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Portal\Chatbot;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class UsersTable extends Entity\DataManager
{
	
	public static function getFilePath()
	{
		return __FILE__;
	}
	
	public static function getTableName()
	{
		return 'mlife_chatbot_users';
	}
	
	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', array(
				'primary' => true,
				'autocomplete' => true,
				)
			),
			new Entity\StringField('USER_KEY', array(
				'required' => true
				)
			),
			new Entity\StringField('SESSION_ID', array(
				'required' => true
				)
			),
			new Entity\StringField('SOURCE', array(
				'required' => true
				)
			),
			new Entity\DatetimeField('DATE_ADD', array(
				'required' => true,
				)
			),
		);
	}
	/*
CREATE TABLE IF NOT EXISTS `mlife_chatbot_users` (
`ID` int(18) NOT NULL AUTO_INCREMENT,
`USER_KEY` varchar(255) DEFAULT NULL,
`SESSION_ID` varchar(255) DEFAULT NULL,
`SOURCE` varchar(65) DEFAULT NULL,
`DATE_ADD` datetime DEFAULT NULL
PRIMARY KEY (`id`)
) AUTO_INCREMENT=1 ;
	*/
}