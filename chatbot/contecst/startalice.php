<?
namespace Mlife\Portal\Chatbot\Contecst;

use Mlife\Portal\Chatbot\Convert as Convert;
use Mlife\Portal\Chatbot\Log as Log;

class StartAlice implements \Mlife\Portal\Chatbot\Interfaces\Contecst{
	
	public static function send($text=''){
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		$command = self::getCommandData($text);
		
		$buttons_viber = array();
		
		$buttons_viber[] = array(
			'title'=>'расписание автобусов',
			'payload'=>'3',
			"BgColor"=> "#eb0b5f",
		);
		$buttons_viber[] = array(
			'title'=>'узнать стоимость рекламы',
			'payload'=>'2',
			"BgColor"=> "#eb0b5f",
		);
		
		$mess = array(
			'text' => 
				'Здравствуйте, Я робот MLife. '."\n".
				'Для продолжения, введите команду: '."\n".
				'2 - узнать стоимость рекламы;'."\n".
				'3 - расписание автобусов;'."\n"
			,
			'tts' => 'Здравствуйте, Я робот Эмлайф. Для продолжения, введите команду.',
			'buttons' => array(),
			'buttons_viber' => $buttons_viber,
		);
		
		if($command == 'ping'){
			return false;
		}elseif($command == '_reklama'){
			
			$session->setContecst('Reklama', true);
			$contecstClass = $session->getContecst();
			return $contecstClass::send('');
			
		}elseif($command == 'start'){
			$command = 'menu';
		}elseif($command == 'help'){
			$mess['text'] = 'Для запуска главного меню воспользуйтесь командой /start, далее следуйте инструкциям чат бота.';
			$mess['tts'] = 'Для запуска главного меню воспользуйтесь командой /start, далее следуйте инструкциям чат бота.';
		}elseif($command == 'settings'){
			$mess['text'] = 'Пока нет настроек для чат бота.';
			$mess['tts'] = 'Пока нет настроек для чат бота.';
		}elseif($command == '_raspis'){
			
			$session->setContecst('Raspis', true);
			$contecstClass = $session->getContecst();
			return $contecstClass::send($text);
			
		}elseif($command == '_raspis_start'){
			
			$session->setParam('LAST_COMMAND','_rasp_start');
			$session->setContecst('Raspis', true);
			$contecstClass = $session->getContecst();
			
			return $contecstClass::send($text);
			
		}elseif(!$command && $session->getParam('LAST_COMMAND') == 'menu'){
			
			$lastMess = $session->getParam('LAST_MESS_DATA');
			$buttons_viber = array();
			if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
			
			$mess = array(
				'text' => 'Не удалось определить команду. Допустимы цифры: 2,3.'."\n".'Попробуйте еще раз.',
				'tts' => 'Не удалось определить команду.',
				'buttons' => array(),
				'buttons_viber'=>$buttons_viber
			);
			$command = 'menu';
		}
		
		if($command) {
			$session->setParam('LAST_COMMAND',$command, true);
		}else{
			$session->setParam('LAST_COMMAND','menu', true);
		}
		
		$session->setParam('LAST_MESS_DATA',$mess,true);
		
		Log::add($mess, 'Сформированный ответ на команду '.$command);
		
		return $mess;
		
	}
	
	public static function getCommandData($text=''){
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		Log::add($text, 'Контекст '.self::class);
		
		$text = Convert::toLowerText($text);
		
		$command = '';
		
		if($text == 'ping') {
			$command = 'ping';
			return $command;
		}elseif(strpos($text,'реклам')!==false){
			if(strpos($text,'стоит')!==false || strpos($text,'стоимость')!==false){
				$command = '_reklama';
			}
		}elseif(strpos($text,'покаж')!==false && strpos($text,'расписание')!==false){
			$command = '_raspis';
		}elseif(strpos($text,'показать')!==false && strpos($text,'расписание')!==false){
			$command = '_raspis';
		}elseif(strpos($text,'подскаж')!==false && strpos($text,'расписание')!==false){
			$command = '_raspis';
		}elseif(strpos($text,'узна')!==false && strpos($text,'расписание')!==false){
			$command = '_raspis';
		}elseif($text == '/start' || $text == 'start' || $text == 'выход в меню' || $text == 'выход'){
			$session->setParam('LAST_COMMAND','',true);
			$command = 'start';
		}elseif($text == '/help' || $text == 'help'){
			$session->setParam('LAST_COMMAND','',true);
			$command = 'help';
		}elseif($text == '/settings' || $text == 'settings'){
			$session->setParam('LAST_COMMAND','',true);
			$command = 'settings';
		}
		
		if($session->getParam('LAST_COMMAND') == 'menu'){
			if($text == '2'){
				$command = '_reklama';
			}elseif($text == '3'){
				$command = '_raspis_start';
			}
		}
		
		Log::add($text, 'Определение команды '.$command);
		
		return $command;
	}
	
}