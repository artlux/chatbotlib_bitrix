<?
namespace Mlife\Portal\Chatbot\Transport;

use Mlife\Portal\Chatbot\Log as Log;

class Vibermain{
	
	public $config = array();
	public $appsConfig = array();
	public $cacheData = array();
	
	function __construct($config = array()) {
		
		$this->config = $config;
		
		try{
			$json = file_get_contents('php://input');
			$jsonData = json_decode($json);
			$this->cacheData = $jsonData;
		
			$appsConfig = Array();
			if (file_exists(DEFAULT_DIR.'/config.php'))
				include(DEFAULT_DIR.'/config.php');
			
			$this->appsConfig = $appsConfig;
			
			if(empty($this->appsConfig)) {
				throw new \Bitrix\Main\ArgumentNullException("appsConfig");
			}
		
		}catch(\Exception $ex){
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			echo $ex->getMessage();
		}
		
	}
	
	public static function getName(){
		return static::class;
	}
	
	public function getConfigSession(){
		
		if(!isset($this->cacheData->sender->id)) return array();
		
		return array(
			'SESSION' => array(
				'ID'=>md5($this->cacheData->sender->id.'_'.$this->appsConfig['botid']).'_'.$this->appsConfig['botid'],
			)
		);
	}
	
	public function getConfigUser(){
		
		if(!isset($this->cacheData->sender->id)) return array();
		
		return array(
			'USER_KEY'=>'viber_'.$this->cacheData->sender->id
		);
	}
	
	public function request($data){
		
		if(empty($data)) $data = $this->cacheData;
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session === null) return $this->sendMessage(false);
		
		if($data->message->text && ($data->event == 'message')){
			
			//подставляем payload для значения текста в кнопке
			$lastMess = $session->getParam('LAST_MESS_DATA');
			$buttons_viber = array();
			$buttons = array();
			if($lastMess['buttons_viber']) $buttons_viber = $lastMess['buttons_viber'];
			if($lastMess['buttons']) $buttons = $lastMess['buttons'];
			
			$buttons_viber[] = array(
				"title"=> 'маршруты',
				"payload"=>'покажи расписание'
			);
			$buttons_viber[] = array(
				"title"=> 'выход в меню',
				"payload"=>'выход'
			);
			
			Log::add($lastMess, 'ImBot lastMess');
			
			foreach($buttons_viber as $b){
				if(trim($b['title']) == trim($data->message->text)) {
					if($b['payload']) {
						$data->message->text = $b['payload'];
						break;
					}
				}
			}
			
			foreach($buttons as $b){
				if(trim($b['title_button']) == trim($data->message->text)) {
					if($b['payload']) {
						$data->message->text = $b['payload'];
						break;
					}
				}
			}
			
			$this->cacheData->message->text = $data->message->text;
			
			$mess = trim($data->message->text);
			
			$contecstClass = $session->getContecst();
			
			if(!$session->getParam('CONTECST')) {
				$session->setContecst('Start', true);
				$contecstClass = $session->getContecst();
			}
			
			if($arMess = \Mlife\Portal\Chatbot\Convert::getGlobalCommand($mess)) {
				$mess = $arMess[0];
				$session->setContecst($arMess[1], true);
				$contecstClass = $session->getContecst();
			}
			
			Log::add($data, 'ImBot Event Query');
			$mess = $contecstClass::send($mess);
			
			if(empty($mess) && $session->getParam('CONTECST') == 'Operator'){
				echo \Mlife\Portal\Chatbot\Convert::resendFromViber();
				return;
			}elseif($session->getParam('CONTECST') == 'Operator'){
				\Mlife\Portal\Chatbot\Convert::resendFromViber();
			}
			
			$result = $this->sendMessage($mess);
		
		}elseif($data->event && ($data->event != 'message')){
			if($session->getParam('CONTECST') == 'Operator'){
				echo \Mlife\Portal\Chatbot\Convert::resendFromViber();
				return;
			}
		}
		
		return $this->sendMessage(false);
		
	}
	
	public function sendMessage($mess){
		
		if($mess === false) {
			echo 'ok';
			return false;
		}
		
		if(!$mess['text']) {
			Log::add($mess, 'message is empty');
			return false;
		}
		
		$mess = $this->prepareData($mess);
		
		$url = 'https://chatapi.viber.com/pa/send_message';
		$zapros = array(
			'receiver'=>$this->cacheData->sender->id,
			'type'=>'text',
			'text'=>$mess['text'],
			'sender'=>array(
				'name'=>'MLifeBot',
			)
		);
		if($mess['keyboard']){
			$zapros['keyboard'] = $mess['keyboard'];
		}else{
			
			$mess['keyboard_fin'] = array(
				"Type"=>"keyboard",
				"Buttons"=>array(
					array(
						"Columns"=> 6,
						"Rows"=> 1,
						"BgColor"=> "#eb0b5f",
						"Text"=> '<font color="#ffffff">выход в меню</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>'выход в меню'
					),
				)
			);
			$zapros['keyboard'] = $mess['keyboard_fin'];
		}
		Log::add($zapros, 'data for telegramm');
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$httpClient->setHeader('X-Viber-Auth-Token', $this->appsConfig['token'], true);
		
		$result = $httpClient->post($url, json_encode($zapros));
		
		Log::add($result, 'result rest telegramm sendMessage');
		
		return $result;
		
	}
	
	private function prepareData($data){
		
		$mess = $data['text'];
		if(!$mess) return $data;
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session->getParam('CONTECST') == 'Raspis'){
		
			$data['keyboard'] = array(
				"Type"=>"keyboard",
				//"DefaultHeight"=> true,
				"Buttons"=>array(
					array(
						"Columns"=> 3,
						"Rows"=> 1,
						"BgColor"=> "#eb0b5f",
						"Text"=> '<font color="#ffffff">маршруты</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>'покажи расписание'
					),
					array(
						"Columns"=> 3,
						"Rows"=> 1,
						"BgColor"=> "#eb0b5f",
						"Text"=> '<font color="#ffffff">выход</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>'выход'
					),
				)
			);
			
			if(!empty($data['buttons_marsh'])){
				
				foreach($data['buttons_marsh'] as $b){
					$data['keyboard']['Buttons'][] = array(
						"Columns"=> 2,
						"Rows"=> 1,
						"BgColor"=> "#0bb0eb",
						"Text"=> '<font color="#ffffff">'.$b['title'].'</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>$b['payload']
					);
				}
			}
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['Buttons'])>40) break;
					$data['keyboard']['Buttons'][] = array(
						"Columns"=> $b['columns'],
						"Rows"=> 1,
						"BgColor"=> "#0bb0eb",
						"Text"=> '<font color="#ffffff">'.$b['title'].'</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>$b['payload']
					);
				}
			}
			
			if(!empty($data['buttons'])){
				$col = 6;
				if(strlen($data['buttons'][0]['title_button'])<6) $col = 2;
				foreach($data['buttons'] as $v){
					$data['keyboard']['Buttons'][] = array(
						"Columns"=> $col,
						"Rows"=> 1,
						"BgColor"=> "#0bb0eb",
						"Text"=> '<font color="#ffffff">'.$v['title_button'].'</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>$v['payload']
					);
				}
				/*$cm = array();
				foreach($data['buttons'] as $v){
					$cm[] = $v['title_comand'];
				}
				$mess .= "\n\nДоступны команды: ".implode('; ',$cm).".";
				*/
			}
			
		}elseif($session->getParam('CONTECST') == 'Reklama'){
				
			$data['keyboard'] = array(
				"Type"=>"keyboard",
				//"DefaultHeight"=> true
			);
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['Buttons'])>40) break;
					$data['keyboard']['Buttons'][] = array(
						"Columns"=> $b['columns'],
						"Rows"=> 1,
						"BgColor"=> ($b['BgColor'] ? $b['BgColor'] : "#0bb0eb"),
						"Text"=> '<font color="#ffffff">'.$b['title'].'</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>$b['payload']
					);
				}
			}
			
		}elseif($session->getParam('CONTECST') == 'Start'){
				
			$data['keyboard'] = array(
				"Type"=>"keyboard",
				//"DefaultHeight"=> true
			);
			
			if(!empty($data['buttons_viber'])){
				foreach($data['buttons_viber'] as $b){
					if(!$b['columns']) $b['columns'] = 6;
					if(count($data['buttons_viber'])>20) $b['columns'] = 3;
					if(count($data['keyboard']['Buttons'])>40) break;
					$data['keyboard']['Buttons'][] = array(
						"Columns"=> $b['columns'],
						"Rows"=> 1,
						"BgColor"=> ($b['BgColor'] ? $b['BgColor'] : "#0bb0eb"),
						"Text"=> '<font color="#ffffff">'.$b['title'].'</font>',
						"TextVAlign"=> "middle",
						"TextHAlign"=> "center",
						"ActionBody"=>$b['payload']
					);
				}
			}
			
		}
		
		
		if($session->getParam('CONTECST') == 'Raspis'){
			$mess .= "\n\nДля завершения диалога отправьте команду: выход.";
		}
		
		if(isset($data['keyboard']) && empty($data['keyboard']['Buttons'])) unset($data['keyboard']);
		
		$data['text'] = $mess;
		
		return $data;
		
	}
	
}