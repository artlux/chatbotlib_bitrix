<?
namespace Mlife\Portal\Chatbot\Contecst;

use Mlife\Portal\Chatbot\Convert as Convert;
use Mlife\Portal\Chatbot\Log as Log;

class Reklama implements \Mlife\Portal\Chatbot\Interfaces\Contecst{
	
	public static function send($text=''){
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		$command = self::getCommandData($text);
		
		$resYandex = \Mlife\Portal\Analitic\YandexTable::getStatsCache();
		$arResult = array(
			'1day'=>$resYandex['CONTENT']['1day']['all']['metrix'][0],
			'30day'=>$resYandex['CONTENT']['30day']['all']['metrix'][0],
			'users'=>$resYandex['CONTENT']['30day']['all']['metrix'][1],
			'banner_count'=>0,
			'banner_click'=>0,
			'banner_pokaz'=>0,
		);
		
		$buttons_viber = array();
		
		$buttons_viber[] = array(
			'title'=>'выход',
			'payload'=>'выход',
			"BgColor"=> "#eb0b5f",
		);
		
		$mess = array(
			'text' => 'Спасибо, за обращение в MLife!'."\n".
				'Стоимость рекламы на портале Мозыря от 6р. в месяц.'."\n\n".
				'Вчера визитов: '.$arResult['1day'].","."\n"."визитов за 30 дней:".$arResult['30day']."\n\n".
				
				'Месячная аудитория: '.$arResult['users']." чел.\n\n".
				
				'Подробную информацию Вы сможете узнать'."\n".
				'по тел: +375(29) 213-33-33,'."\n".
				'либо на странице: https://reklama.mlife.by/'."\n\n".
				'для завершения диалога отправьте команду: выход'."\n\n".
				'Остались вопросы? Напишите любой текст и я направлю Вас на свободного оператора.'
				,
			'tts' => 'Спасибо, за обращение в эмлайф. Стоимость рекламы на портале от 6 рублей в месяц.',
			'buttons' => array(),
			'buttons_viber' => $buttons_viber,
		);
		
		$t = \Mlife\Portal\Chatbot\Main::$transport;
		if(strpos($t::getName(),'Alisa')!==false){
			$mess = array(
				'text' => 'Спасибо, за обращение в MLife!'."\n".
					'Стоимость рекламы на портале Мозыря от 6р. в месяц.'."\n\n".
					'Вчера визитов: '.$arResult['1day'].","."\n"."визитов за 30 дней:".$arResult['30day']."\n\n".
					
					'Месячная аудитория: '.$arResult['users']." чел.\n\n".
					
					'Подробную информацию Вы сможете узнать'."\n".
					'по тел: +375(29) 213-33-33,'."\n".
					'либо на странице: https://reklama.mlife.by/'."\n\n".
					'для завершения диалога отправьте команду: выход'
					,
				'tts' => 'Спасибо, за обращение в эмлайф. Стоимость рекламы на портале от 6 рублей в месяц.',
				'buttons' => array(),
				'buttons_viber' => $buttons_viber,
			);
		}
		
		if($command == 'ping'){
			return false;
		}elseif($command == '_exit'){
			
			$buttons_viber = array();
			
			$mess = array(
				'text' => 'Диалог c ботом завершен.'."\n".'Обращайтесь еще, будем рады помочь.',
				'tts' => 'Обращайтесь еще, будем рады помочь.',
				'buttons' => array(),
				'buttons_viber' => $buttons_viber,
			);
			$session->setContecst('Start', true);
		}elseif($command == '_operator'){
			$session->setContecst('Operator', true);
			$contecstClass = $session->getContecst();
			$session->setParam('LAST_COMMAND','_operator', true);
			return $contecstClass::send($text);
		}
		
		if($command) {
			$session->setParam('LAST_COMMAND',$command, true);
		}else{
			//$session->setParam('LAST_COMMAND','', true);
		}
		
		$session->setParam('LAST_MESS_DATA',$mess,true);
		
		Log::add($mess, 'Сформированный ответ на команду '.$command);
		
		return $mess;
		
	}
	
	public static function getCommandData($text=''){
		Log::add($text, 'Контекст '.self::class);
		
		$text = Convert::toLowerText($text);
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		$command = '';
		
		if($text == 'ping') {
			$command = 'ping';
			return $command;
		}elseif($text=='выход') {
			$command = '_exit';
		}elseif($text == '-'){
			$command = '_exit';
		}elseif($text){
			$t = \Mlife\Portal\Chatbot\Main::$transport;
			//Log::add($t::getName(), 'Определение команды transportName'.$command);
			if(strpos($t::getName(),'Alisa')!==false){
				$command = '_exit';
			}else{
				$command = '_operator';
			}
		}
		
		Log::add($text, 'Определение команды '.$command);
		return $command;
	}
	
}