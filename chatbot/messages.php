<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.portal
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Portal\Chatbot;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class MessagesTable extends Entity\DataManager
{
	
	public static function getFilePath()
	{
		return __FILE__;
	}
	
	public static function getTableName()
	{
		return 'mlife_chatbot_messages';
	}
	
	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', array(
				'primary' => true,
				'autocomplete' => true,
				)
			),
			new Entity\IntegerField('USER_ID', array(
				'required' => true
				)
			),
			new Entity\StringField('MESS', array(
				'required' => true,
				'serialized' => true,
				)
			),
			new Entity\StringField('TYPE', array(
				'required' => true
				)
			),
			new Entity\DatetimeField('DATE_ADD', array(
				'required' => true,
				)
			)
		);
	}
	/*
CREATE TABLE IF NOT EXISTS `mlife_chatbot_messages` (
`ID` int(18) NOT NULL AUTO_INCREMENT,
`USER_ID` int(18) DEFAULT NULL,
`MESS` varchar(2655) DEFAULT NULL,
`TYPE` varchar(65) DEFAULT NULL,
`DATE_ADD` datetime DEFAULT NULL
PRIMARY KEY (`id`)
) AUTO_INCREMENT=1 ;
	*/
}