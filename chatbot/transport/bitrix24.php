<?
namespace Mlife\Portal\Chatbot\Transport;

use Mlife\Portal\Chatbot\Log as Log;

class Bitrix24 implements \Mlife\Portal\Chatbot\Interfaces\Transport{
	
	public $config = array();
	public $appsConfig = array();
	public $cacheData = array();
	
	function __construct($config = array()) {
		
		$this->config = $config;
		
		$appsConfig = Array();
		if (file_exists(DEFAULT_DIR.'/config.php'))
			include(DEFAULT_DIR.'/config.php');
		
		$this->appsConfig = $appsConfig;
		
		$this->cacheData = $_REQUEST;
		
	}
	
	public static function getName(){
		return static::class;
	}
	
	public function getConfigSession(){
		
		if(!isset($this->cacheData['data']['PARAMS']['CHAT_ENTITY_ID'])) return array();
		
		return array(
			'SESSION' => array(
				'ID'=>md5($this->cacheData['data']['PARAMS']['CHAT_ENTITY_ID']),
			)
		);
	}
	
	public function getConfigUser(){
		
		if(!isset($this->cacheData['data']['PARAMS']['CHAT_ENTITY_ID'])) return array();
		
		return array(
			'USER_KEY'=>str_replace('|','_',$this->cacheData['data']['PARAMS']['CHAT_ENTITY_ID'])
		);
	}
	
	public function request($data){
		
		if(empty($data)) $data = $this->cacheData;
		
		if($data['debug']){
			$data = 'a:4:{s:5:"event";s:17:"ONIMBOTMESSAGEADD";s:4:"data";a:3:{s:3:"BOT";a:1:{i:1847;a:6:{s:6:"domain";s:23:"mlife-media.bitrix24.ru";s:9:"member_id";s:32:"319f050d69819a033985fd43dd88b965";s:17:"application_token";s:32:"a424331ee56bcbc480a7dd93bf4ca700";s:4:"AUTH";a:3:{s:6:"domain";s:23:"mlife-media.bitrix24.ru";s:9:"member_id";s:32:"319f050d69819a033985fd43dd88b965";s:17:"application_token";s:32:"a424331ee56bcbc480a7dd93bf4ca700";}s:6:"BOT_ID";s:4:"1847";s:8:"BOT_CODE";s:9:"mlife_bot";}}s:6:"PARAMS";a:22:{s:12:"FROM_USER_ID";s:2:"44";s:7:"MESSAGE";s:1:"3";s:10:"TO_CHAT_ID";s:2:"78";s:12:"MESSAGE_TYPE";s:1:"L";s:12:"SKIP_COMMAND";s:1:"N";s:14:"SKIP_CONNECTOR";s:1:"N";s:19:"IMPORTANT_CONNECTOR";s:1:"N";s:16:"SILENT_CONNECTOR";s:1:"N";s:9:"AUTHOR_ID";s:2:"44";s:7:"CHAT_ID";s:2:"78";s:14:"CHAT_AUTHOR_ID";s:1:"5";s:16:"CHAT_ENTITY_TYPE";s:5:"LINES";s:14:"CHAT_ENTITY_ID";s:20:"vkgroup|2|6769402|44";s:18:"CHAT_ENTITY_DATA_1";s:27:"N|NONE|0|N|N|945|1524740735";s:18:"CHAT_ENTITY_DATA_3";s:1:"N";s:15:"COMMAND_CONTEXT";s:8:"TEXTAREA";s:16:"MESSAGE_ORIGINAL";s:1:"3";s:10:"TO_USER_ID";s:1:"0";s:9:"DIALOG_ID";s:6:"chat78";s:10:"MESSAGE_ID";s:6:"316581";s:9:"CHAT_TYPE";s:1:"L";s:8:"LANGUAGE";s:2:"ru";}s:4:"USER";a:10:{s:2:"ID";s:2:"44";s:4:"NAME";s:33:"Андрей Загальский";s:10:"FIRST_NAME";s:12:"Андрей";s:9:"LAST_NAME";s:20:"Загальский";s:13:"WORK_POSITION";s:0:"";s:6:"GENDER";s:1:"M";s:6:"IS_BOT";s:1:"N";s:12:"IS_CONNECTOR";s:1:"Y";s:10:"IS_NETWORK";s:1:"N";s:11:"IS_EXTRANET";s:1:"Y";}}s:2:"ts";s:10:"1524825661";s:4:"auth";a:12:{s:12:"access_token";s:70:"4d0ce35a0018c34c00002c0f0000002c00000371693affc59e3c3c2a9250254d6082b9";s:7:"expires";s:10:"1524829261";s:10:"expires_in";s:4:"3600";s:5:"scope";s:81:"crm,imopenlines,mailservice,user,messageservice,imbot,task,tasks_extended,im,disk";s:6:"domain";s:23:"mlife-media.bitrix24.ru";s:15:"server_endpoint";s:31:"https://oauth.bitrix.info/rest/";s:6:"status";s:1:"L";s:15:"client_endpoint";s:37:"https://mlife-media.bitrix24.ru/rest/";s:9:"member_id";s:32:"319f050d69819a033985fd43dd88b965";s:7:"user_id";s:2:"44";s:13:"refresh_token";s:70:"3e8b0a5b0018c34c00002c0f0000002c000003604ca6aaf9a73f8dbd6fd45921463edb";s:17:"application_token";s:32:"a424331ee56bcbc480a7dd93bf4ca700";}}';
			$data = unserialize($data);
		}
		
		Log::add($data, 'ImBot Event Query');
		//Log::add(serialize($data), 'ImBot Event Query serialized');
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if ($data['event'] == 'ONAPPINSTALL'){
			
			return $this->install($data);
			
		}elseif($data['event'] == 'ONIMBOTJOINCHAT'){
			
			if (!isset($this->appsConfig[$data['auth']['application_token']]))
				return false;

			if ($data['data']['PARAMS']['CHAT_ENTITY_TYPE'] != 'LINES')
				return false;
			
			/*
			$resultUser = $this->restCommand("user.get", array(
				'ID'=>$data['auth']['user_id']
			), $data["auth"]);
			
			Log::add($resultUser, 'result rest resultUser');
			*/
			
			/*
			$session->setParam('LAST_COMMAND','', true);
			$session->setContecst('Start');
			$contecstClass = $session->getContecst();
			$mess = $contecstClass::send();
			$result = $this->sendMessage($mess);
			*/
			
		}elseif($data['event'] == 'ONIMBOTMESSAGEADD'){
			
			if (!isset($this->appsConfig[$data['auth']['application_token']]))
				return false;

			if ($data['data']['PARAMS']['CHAT_ENTITY_TYPE'] != 'LINES')
				return false;
				
			if ($data['data']['PARAMS']['SKIP_CONNECTOR'] == 'Y')
				return false;
			
			if($data['data']["USER"]["NAME"] == 'Гость'){
				$result = $this->restCommand("user.update", array(
					'ID'=>$data['data']["USER"]["ID"],
					'NAME'=>'Пользователь',
					'LAST_NAME'=>'Пользователь',
				), $data["auth"]);
			}
			
			$session = \Mlife\Portal\Chatbot\Main::$user;
			
				//сообщение от пользователя
			if($data['data']['USER']['IS_CONNECTOR']=='Y'){
				$mess = trim($data['data']['PARAMS']['MESSAGE']);
				
				$prepareParam = explode('|',$data['data']['PARAMS']['CHAT_ENTITY_ID']);
				
				if($prepareParam[0]=='vkgroup'){
					$session->setContecst('Operator');
				}
				if($prepareParam[0]=='telegrambot'){
					$session->setContecst('Operator');
				}
				if($prepareParam[0]=='viber'){
					$session->setContecst('Operator');
				}
				
				$contecstClass = $session->getContecst();
				$mess = $contecstClass::send($mess);
				$result = $this->sendMessage($mess);
				
			}else{
				//сообщение от оператора
				$prepareParam = explode('|',$data['data']['PARAMS']['CHAT_ENTITY_ID']);
				if($prepareParam[1] == 2){
					$sessionKey = false;
					$sessionDir = false;
					if($prepareParam[0]=='vkgroup'){
						$sessionKey = md5($prepareParam[2].'_13502780').'_13502780';
						$sessionDir = $_SERVER["DOCUMENT_ROOT"].'/api/vk_api_new_mlife';
					}
					if($prepareParam[0]=='telegrambot'){
						$sessionKey = md5($prepareParam[2].'_mlifebot').'_mlifebot';
						$sessionDir = $_SERVER["DOCUMENT_ROOT"].'/api/telegram_api_mlife';
					}
					if($prepareParam[0]=='viber'){
						$sessionKey = md5($prepareParam[2].'_mlifebot').'_mlifebot';
						$sessionDir = $_SERVER["DOCUMENT_ROOT"].'/api/viber_api_mlife';
					}
					
					if($sessionKey){
						$fileName = $sessionDir.'/cache/'.$sessionKey.'.cache';
						if (file_exists($fileName)){
							$sParams = unserialize(file_get_contents($sessionDir.'/cache/'.$sessionKey.'.cache'));
							
							if($sParams['CONTECST'] != 'Operator'){
								$sParams['CONTECST'] = 'Operator';
								$sParams['SESSION_EXPIRED'] = $session->getParam('SESSION_EXPIRED');
								
								$config = serialize($sParams);
								file_put_contents($sessionDir.'/cache/'.$sessionKey.'.cache', $config);
								chmod($sessionDir.'/cache/'.$sessionKey.'.cache', 0744);
								
								//отправка сообщение о завершении диалога с ботом оператором
								$mess = array(
									'text'=>'Оператор завершил Ваш диалог с чат ботом.'."\n".
									'Внимание! Все последующие сообщения не будут обрабатываться чат ботом, пока вы не выйдете в главное меню.'."\n\n".
									'Для завершения диалога с оператором отправьте команду: выход',
									'tts'=>'',
									'buttons'=>array()
								);
								$result = $this->sendMessage($mess);
								
							}
							
						}
					}
					
				}
			}
			
		}
		
	}
	
	private function prepareData($data){
		
		$mess = $data['text'];
		if(!$mess) return $data;
		
		if(!empty($data['buttons'])){
			$cm = array();
			foreach($data['buttons'] as $v){
				$cm[] = $v['title_comand'];
			}
			$mess .= "\n\nДоступны команды: ".implode('; ',$cm).".";
		}
		
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session->getParam('CONTECST') == 'Raspis'){
			$mess .= "\n\nДля завершения диалога отправьте команду: выход.";
		}
		
		$data['text'] = $mess;
		
		return $data;
		
	}
	
	public function sendMessage($mess = false){
		
		$data = $this->cacheData;
		
		if(!$data['data']['PARAMS']['DIALOG_ID']) {
			Log::add(array(), 'dialog not found');
			return false;
		}
		
		if(!$mess['text']) {
			Log::add($mess, 'message is empty');
			return false;
		}
		
		$mess = $this->prepareData($mess);
		
		$result = $this->restCommand('imbot.message.add', Array(
			"DIALOG_ID" => $data['data']['PARAMS']['DIALOG_ID'],
			"MESSAGE" => $mess['text'],
		), $data["auth"]);
		
		Log::add($result, 'result rest sendMessage');
		
		return $result;
	}
	
	private function install($data){
	
		$handlerBackUrl = ($_SERVER['SERVER_PORT']==443||$_SERVER["HTTPS"]=="on"? 'https': 'http')."://".$_SERVER['SERVER_NAME'].(in_array($_SERVER['SERVER_PORT'], Array(80, 443))?'':':'.$_SERVER['SERVER_PORT']).$_SERVER['SCRIPT_NAME'];

		$result = $this->restCommand('imbot.register', Array(
			'CODE' => 'mlife_bot',
			'TYPE' => 'O',
			'EVENT_MESSAGE_ADD' => $handlerBackUrl,
			'EVENT_WELCOME_MESSAGE' => $handlerBackUrl,
			'EVENT_BOT_DELETE' => $handlerBackUrl,
			'OPENLINE' => 'Y',
			'PROPERTIES' => Array(
				'NAME' => 'MLifeBot #'.(count($this->appsConfig)+1),
				'WORK_POSITION' => "Бот для портала mlife.by",
				'COLOR' => 'GREEN',
				'PERSONAL_PHOTO' => base64_encode(file_get_contents(DEFAULT_DIR.'/avatar.png')),
			)
		), $data["auth"]);
		$botId = $result['result'];

		$result = $this->restCommand('event.bind', Array(
			'EVENT' => 'OnAppUpdate',
			'HANDLER' => $handlerBackUrl
		), $data["auth"]);

		$this->appsConfig[$data['auth']['application_token']] = Array(
			'BOT_ID' => $botId,
			'LANGUAGE_ID' => $data['data']['LANGUAGE_ID'],
			'AUTH' => $data['auth'],
		);
		$this->saveParams($this->appsConfig);

		Log::add(Array($botId), 'ImBot register');
		
		return $result;
	}
	
	private function restCommand($method, array $params = Array(), array $auth = Array(), $authRefresh = true){
		$queryUrl = "https://".$auth["domain"]."/rest/".$method;
		$queryData = http_build_query(array_merge($params, array("auth" => $auth["access_token"])));

		Log::add(Array('URL' => $queryUrl, 'PARAMS' => array_merge($params, array("auth" => $auth["access_token"]))), 'ImBot send data');

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => 1,
			CURLOPT_URL => $queryUrl,
			CURLOPT_POSTFIELDS => $queryData,
		));

		$result = curl_exec($curl);
		curl_close($curl);

		$result = json_decode($result, 1);

		if ($authRefresh && isset($result['error']) && in_array($result['error'], array('expired_token', 'invalid_token')))
		{
			$auth = $this->restAuth($auth);
			if ($auth)
			{
				$result = $this->restCommand($method, $params, $auth, false);
			}
		}

		return $result;
	}
	
	private function restAuth($auth){
		if (!$this->config['CLIENT_ID'] || !$this->config['CLIENT_SECRET'])
			return false;

		if(!isset($auth['refresh_token']) || !isset($auth['scope']) || !isset($auth['domain']))
			return false;

		$queryUrl = 'https://'.$auth['domain'].'/oauth/token/';
		$queryData = http_build_query($queryParams = array(
			'grant_type' => 'refresh_token',
			'client_id' => $this->config['CLIENT_ID'],
			'client_secret' => $this->config['CLIENT_SECRET'],
			'refresh_token' => $auth['refresh_token'],
			'scope' => $auth['scope'],
		));

		Log::add(Array('URL' => $queryUrl, 'PARAMS' => $queryParams), 'ImBot request auth data');

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $queryUrl.'?'.$queryData,
		));

		$result = curl_exec($curl);
		curl_close($curl);

		$result = json_decode($result, 1);
		if (!isset($result['error']))
		{
			$result['application_token'] = $auth['application_token'];
			$this->appsConfig[$auth['application_token']]['AUTH'] = $result;
			$this->saveParams($this->appsConfig);
		}
		else
		{
			$result = false;
		}

		return $result;
	}
	
	private function saveParams($params){
		$config = "<?php\n";
		$config .= "\$appsConfig = ".var_export($params, true).";\n";
		$config .= "?>";

		file_put_contents(DEFAULT_DIR."/config.php", $config);

		return true;
	}
	
}