<?
namespace Mlife\Portal\Chatbot\Transport;

use Mlife\Portal\Chatbot\Log as Log;

class Vkgroup{
	
	public $config = array();
	public $appsConfig = array();
	public $cacheData = array();
	
	function __construct($config = array()) {
		
		$this->config = $config;
		
		try{
			$json = file_get_contents('php://input');
			$jsonData = json_decode($json);
			$this->cacheData = $jsonData;
		
			$appsConfig = Array();
			if (file_exists(DEFAULT_DIR.'/config.php'))
				include(DEFAULT_DIR.'/config.php');
			
			$arParams = array();
			if($this->cacheData->group_id && isset($appsConfig[$this->cacheData->group_id])){
				$arParams = $appsConfig[$this->cacheData->group_id];
			}
			
			$this->appsConfig = $arParams;
			
			if(empty($this->appsConfig)) {
				throw new \Bitrix\Main\ArgumentNullException("appsConfig");
			}
		
		}catch(\Exception $ex){
			header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
			echo $ex->getMessage();
		}
		
	}
	
	public static function getName(){
		return static::class;
	}
	
	public function getConfigSession(){
		
		if(!isset($this->cacheData->object->user_id)) return array();
		
		return array(
			'SESSION' => array(
				'ID'=>md5($this->cacheData->object->user_id.'_'.$this->cacheData->group_id).'_'.$this->cacheData->group_id,
			)
		);
	}
	
	public function getConfigUser(){
		
		if(!isset($this->cacheData->object->user_id)) return array();
		
		return array(
			'USER_KEY'=>'vk_'.$this->cacheData->object->user_id
		);
	}
	
	public function request($data){
		
		if(empty($data)) $data = $this->cacheData;
		
		if($data->type == 'confirmation'){
			echo $this->appsConfig['confirmation'];
			return;
		}
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($data->type = 'message_new' && !$data->object->out && $this->appsConfig['secret']==$data->secret){
			
			if($session->getParam('CONTECST')!='Raspis'){
				$session->lock();
			}
			
			$mess = trim($data->object->body);
			
			$contecstClass = $session->getContecst();
			
			if(!$session->getParam('CONTECST')) {
				$session->setContecst('Start', true);
				$contecstClass = $session->getContecst();
			}
			
			if($session->getParam('CONTECST') == 'StartVkGroup'){
				$resStart = $contecstClass::parceStartCommand($mess, $this->appsConfig);
				if($resStart){
					Log::add($data, 'ImBot Event Query');
					$session->unlock();
					$session->setContecst('Raspis',true);
					
					$contecstClass = $session->getContecst();
					$mess = $contecstClass::send($mess);
					$result = $this->sendMessage($mess);
				}
			}else{
				Log::add($data, 'ImBot Event Query');
				$mess = $contecstClass::send($mess);
				$result = $this->sendMessage($mess);
			}
			
			
		}
		
		return $this->sendMessage(false);
		
	}
	
	public function sendMessage($mess){
		
		if($mess === false) {
			echo 'ok';
			return false;
		}
		
		if(!$mess['text']) {
			Log::add($mess, 'message is empty');
			return false;
		}
		
		$mess = $this->prepareData($mess);
		
		$url = 'https://api.vk.com/method/messages.send';
		$zapros = array(
			'access_token'=>$this->appsConfig['token'],
			'v'=>'5.38',
			'user_id'=>$this->cacheData->object->user_id,
			'peer_id'=>'-'.$this->cacheData->group_id,
			'message'=>$mess['text']
		);
		$httpClient = new \Bitrix\Main\Web\HttpClient();
		$result = $httpClient->post($url, $zapros);
		
		Log::add($result, 'result rest vk.com sendMessage');
		
		return $result;
		
	}
	
	private function prepareData($data){
		
		$mess = $data['text'];
		if(!$mess) return $data;
		
		if(!empty($data['buttons'])){
			$cm = array();
			foreach($data['buttons'] as $v){
				$cm[] = $v['title_comand'];
			}
			$mess .= "\n\nДоступны команды: ".implode('; ',$cm).".";
		}
		
		
		$session = \Mlife\Portal\Chatbot\Main::$user;
		
		if($session->getParam('CONTECST') == 'Raspis'){
			$mess .= "\n\nДля завершения диалога отправьте команду: выход.";
		}
		
		$data['text'] = $mess;
		
		return $data;
		
	}
	
	//обработчик на замену контекста
	public static function OnSetContecst(\Bitrix\Main\Event $event){
		$params = $event->getParameters();
		$contecst = $params['CONTECST'];
		if($contecst == 'Start') {
			$contecst = 'StartVkGroup';
			$result = new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::SUCCESS,array('CONTECST'=>$contecst));
			return $result;
		}
	}
	
}